$(document).ready(function () {
    var btn = $('#import_evaluation [type="submit"]');

    paceOptions = {
        catchupTime: 100,
        initialRate: .03,
        minTime: 250,
        ghostTime: 100,
        maxProgressPerFrame: 20,
        easeFactor: 1.25,
        startOnPageLoad: true,
        restartOnPushState: true,
        restartOnRequestAfter: 500,
        target: 'body',
        elements: {
          checkInterval: 100,
          selectors: ['body']
        },
        eventLag: {
          minSamples: 10,
          sampleCount: 3,
          lagThreshold: 3
        },
        ajax: {
          trackMethods: ['POST'],
          trackWebSockets: true,
          ignoreURLs: []
        }
      }

    $('#import_evaluation').validate({
        errorElement: 'span', // default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: '', // validate all fields including form hidden input
        rules: {
            file: {
                required: true
            },

        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group .form-control').addClass('is-invalid') // set invalid class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group .form-control').removeClass('is-invalid') // set invalid class to the control group
                .closest('.form-group .form-control').addClass('is-valid')
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                error.insertAfter(element.parent())
            } else {
                error.insertAfter(element)
            }
        },
        success: function (label) {
            label
                .closest('.form-group .form-control').removeClass('is-invalid') // set success class to the control group
        },
        submitHandler: function (element) {
            swal({
                title: "กรุณารอ!",
                text: "ระบบกำลังนำเข้าข้อมูล",
                imageUrl: 'https://media0.giphy.com/media/sSgvbe1m3n93G/giphy.gif?cid=790b7611d61362dfe867287efcc467e76addd4b1219c4c50&rid=giphy.gif'
            });
            $.ajax({
                type: "post",
                url: rurl + 'admin/import/evaluation',
                data: new FormData(element),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData:false,
                success: function (data) {
                    console.log(data);
                    swal("Import Success!", data.executetime, "success");
                },
                error: function (data) {
                    console.log(data);
                    swal("Import Error!", data, "error");
                }
            }).fail(function (data) {
                console.log(data);
                swal("Import ERROR!", data, "error");
            });
        }
    })
});