var Project = (function () {
    var handleTables = function () {
      var table = $('#project').DataTable({
        "responsive": true,
        "serverSide": true,
        "processing": true,
        "ajax": rurl + 'admin/project/list',
        "language": { "url" : rurl + "assets/plugins/datatable_th.json" },
        "order": [[ 9, "desc" ]],
        "columns": [{
            "data": 'DT_RowIndex',
            "name": 'DT_RowIndex',
            orderable: false,
            searchable: false,
            className:"text-center"
          },
					{"data":"pname","name":"project.name"},
					{"data":"detail","name":"project.detail"},
					{"data":"due_date","name":"project.due_date"},
					{"data":"ename","name":"employee.firstname"},
          {"data":"firstname","name":"employee.firstname","visible":false},
          {"data":"lastname","name":"employee.lastname","visible":false},
          {"data":"assign",orderable: false,searchable: false,},
          {"data":"status","name":"project.status"},
          {"data":"created_at","name":"project.created_at"},
          {
            "data": "action",
            orderable: false,
            searchable: false
          }

        ],
        "dom": "<'row' <'col-6'lB> <'col-6'f> >" + "rt" + "<'row' <'col-6' i><'col-6'p> >",
        "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ],
        "buttons": [
          {
              extend: 'excel',
              exportOptions: {
                  columns: [ [9], ':visible' ]
              },
              className: 'btn btn-default btn-sm',
              text: '<i class="fas fa-file-excel"></i> Excel',
              orientation: 'landscape',
              
              title: 'โปรเจค'
          },
          {
              extend: 'print',
              className: 'btn btn-default btn-sm',
              text: '<i class="fas fa-print"></i> Print',
              orientation: 'landscape',
              title: 'โปรเจค',
              exportOptions: {
                  columns: [ [9], ':visible' ]
              },
          },
      ],
      });
    }
  
    var handleValidation = function () {
      var form = $('.validateForm');
      var btn = $('.validateForm [type="submit"]');

      form.validate({
        errorElement: 'span', // default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: '', // validate all fields including form hidden input

        rules: {

        },
  
        highlight: function (element) { // hightlight error inputs
          $(element)
            .closest('.form-group .form-control').addClass('is-invalid') // set invalid class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
          $(element)
            .closest('.form-group .form-control').removeClass('is-invalid') // set invalid class to the control group
            .closest('.form-group .form-control').addClass('is-valid')
        },
        errorPlacement: function (error, element) {
          if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
            error.insertAfter(element.parent())
          } else {
            error.insertAfter(element)
          }
        },
        success: function (label) {
          label
            .closest('.form-group .form-control').removeClass('is-invalid') // set success class to the control group
        },
        submitHandler: function(element) {
          btn.prop('disabled', true)
          $.ajax({
            type: "post",
            url: rurl+'admin/project',
            data: $( element ).serialize(),
            dataType: "html",
            success: function (data) {
              btn.prop('disabled', false)
              $('[data-dismiss="modal"]').trigger('click');
              $('.validateForm').removeClass('formadd')
              $('.validateForm').removeClass('formedit')
              $("#project").DataTable().ajax.reload(null, false);
              swal('ยินดีด้วย!', data, "success")
            },
            error: function (data) {
              $('.validateForm').removeClass('formadd')
              $('.validateForm').removeClass('formedit')
              
            }
          });
        }
      })
    }

    var handleButton = function () {
        $(document).on('click', '.btn-delete', function(){
          $this = $(this)
          swal({
            title: "คุณแน่ใจไหม?",
            text: "เมื่อลบแล้วคุณจะไม่สามารถกู้คืนไฟล์นี้ได้!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "ใช่, ยืนยัน!",
            cancelButtonText: "ยกเลิก",
            closeOnConfirm: false
          },
          function(){
            deleteUser($this);
          });
        })

        $(document).on('click', '.btn-add', function(){
          $('[name="id"]').val(null);

          //remove form class
          $('.validateForm').removeClass('formadd')
          $('.validateForm').removeClass('formedit')

          $('.validateForm').trigger('reset')
          $('.validateForm').addClass('formadd')
          $('.validateForm').removeAttr('data-id')

          //add value to form example
          $('.ls-select2').select2();
        })

        $('form.manage-people').submit(function (e) { 
          e.preventDefault();
          var data = $('form.manage-people').serialize();
          $.ajax({
            type: 'post',
            url: rurl+'admin/project/assign',
            dataType: "html",
            data: data,
            success: function (response) {
              console.log(response);
              $("#project").DataTable().ajax.reload(null, false);
              $('.modal-manage-people').modal('hide');
              swal('ยินดีด้วย!', response, "success");
            }
          });
        });

        $(document).on('change','[name="assign_people[]"]', function (e) {
          e.preventDefault();
          var items = $(this).find( "option:selected" );
          // console.log();
          $('#project_assign_people').html('');
          $.each(items, function (indexInArray, valueOfElement) { 
            var pictureImage = valueOfElement.dataset.pictureImage;
            console.log(valueOfElement);
            $('#project_assign_people').append(
              '<div class="d-flex justify-content-start align-items-center list-employee">'+
                '<div class="thumbnail-wrapper circular d48 m-r-10 ">'+
                  '<img data-src-retina="'+surl+pictureImage+'" data-src="'+surl+pictureImage+'" alt="" src="'+surl+pictureImage+'" width="43" height="43">'+
                '</div>'+
                '<div class="">'+
                  '<h5 class="name no-margin">'+valueOfElement.text+' <a class="btn-delete-people btn btn-xs btn-danger" data-id="'+valueOfElement.value+'"> x </a></h5>'+
                '</div>'+
              '</div>'
            );
          });
        });

        $(document).on('click','.btn-manage-people', function () {
          var id = $(this).data('id');
          $.ajax({
            type: 'get',
            url: rurl+'admin/project/assign/'+id,
            dataType: "json",
            success: function (data) {
              $('#project_assign_people').html('');
              $('[name="assign_people[]"]').val(data.employees);
              $.each(data.data, function (indexInArray, valueOfElement) {
                $('[name="project_id"]').val(valueOfElement.project_id); 
                $('#project_assign_people').append(
                  '<div class="d-flex justify-content-start align-items-center list-employee">'+
                    '<div class="thumbnail-wrapper circular d48 m-r-10 ">'+
                      '<img data-src-retina="'+surl+valueOfElement.picture_profile+'" data-src="'+surl+valueOfElement.picture_profile+'" alt="" src="'+surl+valueOfElement.picture_profile+'" width="43" height="43">'+
                    '</div>'+
                    '<div class="">'+
                      '<h5 class="name no-margin">'+valueOfElement.ename+' <a class="btn-delete-people btn btn-xs btn-danger" data-id="'+valueOfElement.employee_id+'"> x </a></h5>'+
                    '</div>'+
                  '</div>'
                );
              });
              $('.ls-select2').select2();
              $('.modal-manage-people').modal('show');
            },
            error: function (data) {
              swal("ไม่พบข้อมูลที่ท่านต้องการ!",data.responseTextta, "error");
            }
          })
        });

        $(document).on('click','.btn-delete-people', function (e) {
          e.preventDefault();
          $(this).closest('.list-employee').remove();
          var employee_id = $(this).data('id');
          const array = $('[name="assign_people[]"]').val();
          const index = array.indexOf(employee_id.toString());
          if (index > -1) {
            array.splice(index, 1);
          }
          $('[name="assign_people[]"]').val(array);
          $('.ls-select2').select2();
        });

        $(document).on('click', '.btn-edit', function(btn){
          //remove form class
          $('.validateForm').removeClass('formadd');
          $('.validateForm').removeClass('formedit');

          var id = $(this).data('id');
          $('[name="id"]').val(id);

          var selector = $('.validateForm');
          selector.addClass('formedit');
          selector.find('[type="submit"]').removeAttr('data-id');
          selector.find('[type="submit"]').attr('data-id',id);
          $.ajax({
            type: 'get',
            url: rurl+'admin/project/'+id,
            dataType: "json",
            success: function (data) {
              Object.entries(data).forEach(entry => {
                $('[name="'+entry[0]+'"]').val(entry[1]);
              })
              $('.ls-select2').select2();
              $('.modal-project').modal('show');
            },
            error: function (data) {
              swal("ไม่พบข้อมูลที่ท่านต้องการ!",data.responseTextta, "error");
            }
          })
        })

        $(document).on('click', '.formadd [type="submit"]', function(){
          handleValidation();
        })

        $(document).on('click', '.formedit [type="submit"]', function(e){
          handleValidation();
        })
    }

    var deleteUser = function (value) {
      var id = value.data('id');
      var token = value.data('token');

      $.ajax({
        url: rurl+'admin/project/'+id,
        type: 'DELETE',
        data: {
          _method: 'delete',
          _token: token,
          _id : id
        },
        success: function (data) {
          $("#project").DataTable().ajax.reload(null, false);
          swal('ยินดีด้วย!', data, "success");
        },
        error: function (data) {
          swal("พบข้อผิดผลาด!",data, "error");
        }
      })
    }
  
    return {
      // main function to initiate the module
      init: function () {
        handleTables();
        handleButton();
      }
    }
  })()
  
  jQuery(document).ready(function () {
    Project.init();
  })