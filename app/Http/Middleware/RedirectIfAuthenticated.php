<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard('api')->check()){
            $data['result_code'] = '000';
            $data['result_desc'] = 'Success';
            $data['message'] = "Success";
            $data['status'] = true;
            return response()->json($data, 200);
        }else{
            $data['result_code'] = '001';
            $data['result_desc'] = 'กรุณาลงชื่อเข้าใช้งาน';
            $data['message'] = "กรุณาลงชื่อเข้าใช้งาน";
            $data['status'] = false;
            return response()->json($data, 401);
        }
        return $next($request);
    }
}