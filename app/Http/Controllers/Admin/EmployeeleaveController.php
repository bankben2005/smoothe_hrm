<?php

namespace App\Http\Controllers\Admin;

use App\Enums\LeaveResultStatus;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FunctionController as FC;
use App\Http\Controllers\Traits\CanLeaveTrait;
use App\Models\Branch;
use App\Models\Company;
use App\Models\Department;
use App\Models\Employee;
use App\Models\Employeeleave;
use App\Models\Groups;
use App\Models\Leaveduration;
use App\Models\Leavetype;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Storage;

class EmployeeleaveController extends Controller
{
    use CanLeaveTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$data['leavetype'] = Leavetype::get();
		$data['leaveduration'] = Leaveduration::get();
        $data['employee'] = Employee::get();
        $data['company'] = Company::get();
        $data['branch'] = Branch::get();
        $data['group'] = Groups::get();
        $data['department'] = Department::get();
        $data['menu'] = 'ระบบจัดการการลา';
        return view('admin.employeeleave')->with($data); // admin/employeeleave
    }

    public function index_employee()
    {
		$data['leavetype'] = Leavetype::get();
		$data['leaveduration'] = Leaveduration::get();
		$data['employee'] = Employee::get();
        $data['menu'] = 'ประวัติการลา';
        return view('admin.employeeleave_employee')->with($data); // admin/employeeleave_employee
    }

    public function index_approver()
    {
		$data['leavetype'] = Leavetype::get();
		$data['leaveduration'] = Leaveduration::get();
		$data['employee'] = Employee::get();
        $data['menu'] = 'ประวัติอนุมัติการลา';
        return view('admin.employeeleave_approver')->with($data);
    }

    public function list(Request $request){
        $model = Employeeleave::query();
        $model->leftjoin('leave_type','employee_leave.leave_type_id','leave_type.id');
        $model->leftjoin('leave_duration','employee_leave.leave_duration_id','leave_duration.id');
        $model->leftjoin(\DB::raw('employee ep1'),'employee_leave.employee_id','ep1.id');
        $model->leftjoin(\DB::raw('employee ep2'),'employee_leave.approver_id','ep2.id');
        $model->select([
            'employee_leave.employee_id',
            'employee_leave.created_at as created_at',
            'employee_leave.id as employee_leaveid',
            'employee_leave.start_date',
            'employee_leave.end_date',
            'employee_leave.remark',
            'employee_leave.approved_at',
            'employee_leave.leave_result',
            'ep1.firstname as ep1firstname',
            'ep1.lastname as ep1lastname',
            'ep2.firstname as ep2firstname',
            'ep2.lastname as ep2lastname',
            'leave_type.leave_name',
            'leave_duration.duration_name'
        ]);
        if(isset($request->company_id)){
            $model->where('ep1.company_id',$request->company_id);
        }
        if(isset($request->branch_id)){
            $model->where('ep1.branch_id',$request->branch_id);
        }
        if(isset($request->group_id)){
            $model->where('ep1.group_id',$request->group_id);
        }
        if(isset($request->department_id)){
            $model->where('ep1.department_id',$request->department_id);
        }
        if(isset($request->leave_type_id)){
            $model->where('employee_leave.leave_type_id',$request->leave_type_id);
        }
        if(isset($request->employee_id)){
            $model->where('ep1.id',$request->employee_id);
        }
        if(isset($request->status)){
            $model->where('employee_leave.leave_result',$request->status);
        }
        
        if(isset($request->month)){
            $month = $request->month;
            $model->where(function($query) use ($month) {
                $query->orWhere('employee_leave.start_date', 'like', "$month%")
                      ->orWhere('employee_leave.end_date', 'like', "$month%");
            });
        }
        return  \DataTables::eloquent($model)
                ->addColumn('action',function($rec){
                    $str = '
                    <a class="btn btn-xs btn-warning btn-edit" href="#" data-id="'.$rec->employee_leaveid.'">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->employee_leaveid.'">
                        <i class="fa fa-trash"></i>
                    </a>
                    <a class="btn btn-xs btn-info" href="'.url('export/employee_leave').'?id='.$rec->employee_leaveid.'">
                        <i class="fa fa-file"></i>
                    </a>
                    ';
                    return $str;
                })
                ->editColumn('ep1firstname',function($rec){
                    return $rec->ep1firstname." ".$rec->ep1lastname;
                })
                ->editColumn('ep2firstname',function($rec){
                    return $rec->ep2firstname." ".$rec->ep2lastname;
                })
                ->editColumn('leave_result',function($rec){
                    switch ($rec->leave_result) {
                        case 'N':
                            return '<span class="label label-default">รออนุมัติ</span>';
                            break;
                        case 'T':
                            return '<span class="label label-info">อนุมัติ</span>';
                            break;
                        case 'F':
                            return '<span class="label label-danger">ไม่อนุมัติ</span>';
                            break;
                    }
                })
                ->addColumn('leave_diff',function($rec){
                    return FC::leave_diff($rec->start_date,$rec->end_date,$rec->employee_id);
                })
                ->addIndexColumn()
                ->rawColumns(['action','leave_result'])
                ->toJson();
    }

    public function list_employee(){
        $model = Employeeleave::query();
        $model->leftjoin('leave_type','employee_leave.leave_type_id','leave_type.id');
        $model->leftjoin('leave_duration','employee_leave.leave_duration_id','leave_duration.id');
        $model->leftjoin(\DB::raw('employee ep1'),'employee_leave.employee_id','ep1.id');
        $model->leftjoin(\DB::raw('employee ep2'),'employee_leave.approver_id','ep2.id');
        $model->where('employee_id',\AUTH::guard('admin')->user()->employee_id);
        // {"data":"leave_name","name":"leave_type.leave_name"},
        // {"data":"duration_name","name":"leave_duration.duration_name"},
        // {"data":"start_date","name":"employee_leave.start_date"},
        // {"data":"end_date","name":"employee_leave.end_date"},
        // {"data":"ep1firstname","name":"ep1.firstname"},
        // {"data":"ep1lastname","name":"ep1.lastname","visible":false},
        // {"data":"remark","name":"employee_leave.remark"},
        // {"data":"ep2firstname","name":"ep2.firstname"},
        // {"data":"ep2lastname","name":"ep2.lastname","visible":false},
        // {"data":"approved_at","name":"employee_leave.approved_at"},
        // {"data":"leave_result","name":"employee_leave.leave_result"},
        $model->select([
            'leave_type.leave_name',
            'leave_duration.duration_name',
            'employee_leave.start_date',
            'employee_leave.end_date',
            'ep1.firstname as ep1firstname',
            'ep1.lastname as ep1lastname',
            'ep2.firstname as ep2firstname',
            'ep2.lastname as ep2lastname',
            'employee_leave.approved_at',
            'employee_leave.leave_result',
            'employee_leave.remark'
        ]);
        return  \DataTables::eloquent($model)
                ->addColumn('action',function($rec){
                    $str = '
                    <a class="btn btn-xs btn-warning btn-edit" href="#" data-id="'.$rec->employee_leaveid.'">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->employee_leaveid.'">
                            <i class="fa fa-trash"></i>
                        </a>
                        <a class="btn btn-xs btn-info" href="'.url('export/employee_leave').'?id='.$rec->employee_leaveid.'">
                            <i class="fa fa-file"></i>
                        </a>
                    ';
                    return $str;
                })
                ->editColumn('ep1firstname',function($rec){
                    return $rec->ep1firstname." ".$rec->ep1lastname;
                })
                ->editColumn('ep2firstname',function($rec){
                    return $rec->ep2firstname." ".$rec->ep2lastname;
                })
                ->editColumn('leave_result',function($rec){
                    switch ($rec->leave_result) {
                        case 'N':
                            return '<span class="label label-default">รออนุมัติ</span>';
                            break;
                        case 'T':
                            return '<span class="label label-info">อนุมัติ</span>';
                            break;
                        case 'F':
                            return '<span class="label label-danger">ไม่อนุมัติ</span>';
                            break;
                    }
                })
                ->addIndexColumn()
                ->rawColumns(['action','leave_result'])
                ->toJson();
    }

    public function list_approver(){
        $model = Employeeleave::query();
        $model->leftjoin('leave_type','employee_leave.leave_type_id','leave_type.id');
        $model->leftjoin('leave_duration','employee_leave.leave_duration_id','leave_duration.id');
        $model->leftjoin(\DB::raw('employee ep1'),'employee_leave.employee_id','ep1.id');
        $model->leftjoin(\DB::raw('employee ep2'),'employee_leave.approver_id','ep2.id');
        $model->leftjoin(\DB::raw('employee_leave_approvers au'),'employee_leave.employee_id','au.employee_id');
        $model->where(\DB::raw('au.approvers'),'LIKE','%"'.\AUTH::guard("admin")->user()->employee_id.'"%');
        $model->select([
            'leave_type.*',
            'leave_type.id as leave_typeid',
            'leave_duration.*',
            'leave_duration.id as leave_durationid',
            'ep1.firstname as ep1firstname',
            'ep1.lastname as ep1lastname',
            // \DB::raw('CONCAT(ep1.firstname," ",ep1.lastname) as ep1name'),
            'ep2.firstname as ep2firstname',
            'ep2.lastname as ep2lastname',
            // \DB::raw('CONCAT(ep2.firstname," ",ep2.lastname) as ep2name'),
            'ep1.id as employeeid',
            'employee_leave.*',
            'employee_leave.id as employee_leaveid',
            \DB::raw('au.approvers')
        ]);
        return  \DataTables::eloquent($model)
                ->addColumn('action',function($rec){
                    $str = '
                    <button class="btn btn-sm btn-approve" href="#" data-id="'.$rec->employee_leaveid.'">
                        <i class="far fa-check-square"></i> อนุมัติ
                    </button>';
                    return $str;
                })
                ->editColumn('ep1firstname',function($rec){
                    return $rec->ep1firstname." ".$rec->ep1lastname;
                })
                ->editColumn('ep2firstname',function($rec){
                    return $rec->ep2firstname." ".$rec->ep2lastname;
                })
                ->editColumn('leave_result',function($rec){
                    switch ($rec->leave_result) {
                        case 'N':
                            return '<span class="label label-default">รออนุมัติ</span>';
                            break;
                        case 'T':
                            return '<span class="label label-info">อนุมัติ</span>';
                            break;
                        case 'F':
                            return '<span class="label label-danger">ไม่อนุมัติ</span>';
                            break;
                    }
                })
                ->addIndexColumn()
                ->rawColumns(['action','leave_result'])
                ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     * @return Exception|\Illuminate\Http\Response|string
     * @throws Exception
     */
    public function store(Request $request)
    {
        //convert json value to array
       

        if(empty($request['start_time'])||empty($request['end_time'])){
            $this->setDate($request);
        }else{
            $request['start_time'] = !empty($request['start_time']) ? ' '. $request['start_time'] : " 00:00:00";
            $request['end_time'] = !empty($request['end_time']) ? ' '. $request['end_time'] : " 00:00:00";
            $request['start_date'] = $request['start_date'].$request['start_time'];
            $request['start_date'] = Carbon::make($request['start_date'])->format(Carbon::DEFAULT_TO_STRING_FORMAT);
            $request['end_date'] = $request['end_date'].$request['end_time'];
            $request['end_date'] = Carbon::make($request['end_date'])->format(Carbon::DEFAULT_TO_STRING_FORMAT);
        }

        unset($request['start_time']);
        unset($request['end_time']);
        
        if(empty($request->id)){
            $request['created_at'] = date("Y-m-d h:i:s");
            if($request->approved_at && $request->approved_time){
                $request['approved_at'] = $request->approved_at." ".$request->approved_time;
            }else{
                $request['approved_at'] = date('Y-m-d H:i:s');
            }
            unset($request['id']);
            unset($request['approved_time']);
            // echo '<PRE>';
            // print_r($request);exit();
            if(empty($request->leave_result)){ $request['leave_result'] = 'N'; }
            \DB::beginTransaction();
            //$picture_arr = $this->image_action($request);
            $picture_arr = array();
            if($request->hasFile('file')){
                foreach($request->file('file') as $key =>  $file){
        
                    if(!empty($file)){
                        $file_name = $file->getClientOriginalName();
                        if(!empty($file_name)){
                            $original_name = $file_name;
                            $extension = $file->extension();
                        
                            if(!empty($request['picture_current']) && $request['picture_current'] != null){
                                $time_stamp = Carbon::now()->timestamp;
                                foreach($request['picture_current'] as $pickey => $picvalue){
                                    if($picvalue == $original_name){
                                        $new_file_name = $time_stamp.rand(0,1000).".".$extension;
                                        $picture_arr[] = $new_file_name;
                                        Storage::disk('leave_image')->put($new_file_name, file_get_contents($file));
                                    }
                                }
                                array_merge($request['picture_current'],$picture_arr);
                            }
                        }
                    }
                }
                
        }

            $request['picture'] = !empty($request['picture_current'])?json_encode($picture_arr):NULL;
            
            unset($request['picture_current']);
            unset($request['max_file_count']);
         
            try {
                // if($result = Employeeleave::insert($request->all())){
                //     \DB::commit();
                //     return "บันทึกข้อมูลสำเร็จ!";
                // }else{
                //     throw new Exception('Error! Processing', 1);
                // }
               
                if($request->hasFile('file')){
                    if(Employeeleave::insert($request->except('file'))){
                        \DB::commit();
                        
                        return "บันทึกข้อมูลสำเร็จ!";
                    }else{
                        // throw new Exception("Error Processing Request", $e);
                    }
                }else{
                   
                    if(Employeeleave::insert($request->all())){
                        \DB::commit();
                    
                        return "บันทึกข้อมูลสำเร็จ!";
                    }else{
                        // throw new Exception("Error Processing Request", $e);
                    }
                }


            } catch (Exception $e) {
                \DB::rollBack();
                return $e;
            }
        }else{
            return $this->update($request,$request->id);
        }
    }
    
    public function image_action($request){
      
        return $request['picture_current'];
    
    }
    // public function store(Request $request)
    // {
    //     //convert json value to array
    //     // print_r($request);exit();
    //     return $request->all();
    //     $request = isset($_POST) ? $_POST : "";
    
    //     $path = $request['files']->store('public/photos/images_leave');
   
 
    //     if(empty($request['start_time'])||empty($request['end_time'])){
    //         $this->setDate($request);
    //     }else{
    //         $request['start_time'] = !empty($request['start_time']) ? ' '. $request['start_time'] : " 00:00:00";
    //         $request['end_time'] = !empty($request['end_time']) ? ' '. $request['end_time'] : " 00:00:00";
    //         $request['start_date'] = $request['start_date'].$request['start_time'];
    //         $request['start_date'] = Carbon::make($request['start_date'])->format(Carbon::DEFAULT_TO_STRING_FORMAT);
    //         $request['end_date'] = $request['end_date'].$request['end_time'];
    //         $request['end_date'] = Carbon::make($request['end_date'])->format(Carbon::DEFAULT_TO_STRING_FORMAT);
    //     }

    //     unset($request['start_time']);
    //     unset($request['end_time']);
        
    //     if(empty($request['id'])){
    //         $request['created_at'] = date("Y-m-d h:i:s");
    //         if($request['approved_at'] && $request['approved_time']){
    //             $request['approved_at'] = $request['approved_at']." ".$request['approved_time'];
    //         }else{
    //             $request['approved_at'] = date('Y-m-d H:i:s');
    //         }
    //         unset($request['id']);
    //         unset($request['approved_time']);

           
    //         //$path = str_replace("public", "/storage", $path);
    //         if(empty($request['leave_result'])){ $request['leave_result'] = 'N'; }
    //         \DB::beginTransaction();
    //         try {
    //             if($result = Employeeleave::insert($request)){
    //                 \DB::commit();
    //                 return "บันทึกข้อมูลสำเร็จ!";
    //             }else{
    //                 throw new Exception('Error! Processing', 1);
    //             }
    //         } catch (Exception $e) {
    //             \DB::rollBack();
    //             return $e;
    //         }
    //     }else{
    //         return $this->update($request,$request['id']);
    //     }
    // }

    /**
     * @param Request $request
     * @throws Exception
     */
    private function setDate(Request $request)
    {
        try {
            $leaveDuration = Leaveduration::query()->find($request->get('leave_duration_id'));

            $startDuration = Carbon::make($leaveDuration->duration_start);
            $endDuration = Carbon::make($leaveDuration->duration_end);

            $request->request->add(
                [
                    'start_date' => Carbon::make($request->get('start_date'))
                        ->setTime($startDuration->hour, $startDuration->minute, $startDuration->second)
                        ->format(Carbon::DEFAULT_TO_STRING_FORMAT),
                    'end_date' => Carbon::make($request->get('end_date'))
                        ->setTime($endDuration->hour, $endDuration->minute, $endDuration->second)
                        ->format(Carbon::DEFAULT_TO_STRING_FORMAT),
                ]
            );
        } catch (Exception $exception) {
            throw new Exception('ไม่พบข้อมูลวันช่วงเวลาที่เลือก');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if($result = Employeeleave::find($id)){
                $start_date = $result['start_date'];
                $end_date = $result['end_date'];
                $result['start_date'] = \App\Http\Controllers\FunctionController::date_format_date($start_date);
                $result['start_time'] = \App\Http\Controllers\FunctionController::date_format_time($start_date);
                $result['end_date'] = \App\Http\Controllers\FunctionController::date_format_date($end_date);
                $result['end_time'] = \App\Http\Controllers\FunctionController::date_format_time($end_date);
                $result['approved_at'] = \App\Http\Controllers\FunctionController::date_format_datetime($result['approved_at']);
                return $result;
            }else{
                throw new Exception('Error! Processing');
            }
        } catch (Exception $e) {
            return $e;
        }
    }

    public function show_detail($id)
    {
        try {
            $result['employeeleave'] = Employeeleave::where('employee_leave.id',$id)
            ->leftjoin('leave_type','leave_type.id','employee_leave.leave_type_id')
            ->leftjoin('leave_duration','leave_duration.id','employee_leave.leave_duration_id')
            ->select(['employee_leave.*','leave_type.leave_name','leave_duration.duration_name'])
            ->first();
            if($result['employeeleave']){
                $result['employee'] = Employee::where('employee.id',$result['employeeleave']->employee_id)
                ->leftjoin('branch','branch.id','employee.branch_id')
                ->leftjoin('level','level.id','employee.level_id')
                ->select(['employee.*','branch.branch_name as bname','level.name as lname'])
                ->first();
                return $result;
            }else{
                throw new Exception('Error! Processing');
            }
        } catch (Exception $e) {
            return $e;
        }
    }

    public function show_approvers($id)
    {
        $result = Employee::where('employee.id',$id)
        ->leftjoin('employee_leave_approvers','employee_leave_approvers.employee_id','employee.id')
        ->select('employee_leave_approvers.*')
        ->first();
        if(is_array(json_decode($result->approvers))){
            return Employee::whereIn('employee.id',json_decode($result->approvers))->get();
        }else{
            return 0;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    $input = $request->all();
    $picture_remove = isset($input['picture_remove'])?($input['picture_remove']):[];


    $max_file_count = $request->max_file_count;
   
     if(isset($request->max_file_count)){
         unset($request->max_file_count);
     }
     if(isset($request['max_file_count'])){
        unset($request['max_file_count']);
    }
    $file_arr = array();
    $count =0;
 
    $picture_arr = array();
    if($request->hasFile('file')){
        foreach($request->file('file') as $key =>  $file){

            if(!empty($file)){
                $file_name = $file->getClientOriginalName();
                if(!empty($file_name)){
                    $original_name = $file_name;
                    $extension = $file->extension();
                
                    if(!empty($request['picture_current']) && $request['picture_current'] != null){
                        $time_stamp = Carbon::now()->timestamp;
                        $keyExcept= array();
                        foreach($request['picture_current'] as $pickey => $picvalue){
                            if($picvalue == $original_name){
                                $new_file_name = $time_stamp.rand(0,1000).".".$extension;
                                $picture_arr[] = $new_file_name;
                                $keyExcept[] = $pickey;
                                Storage::disk('leave_image')->put($new_file_name, file_get_contents($file));
                            }
                        }
                        /** combine two array  */
                        $implode_ = "";
                        $requestPictureCurrentArr = $request['picture_current'];
                        foreach($keyExcept as $key => $expv){
                            unset($requestPictureCurrentArr[$expv]);
                        }
                       
                        //$merge = array_merge($requestPictureCurrentArr,$picture_arr);
                        $merge = array_unique(array_merge($requestPictureCurrentArr,$picture_arr), SORT_REGULAR);
                        $request['picture_current'] = array_values($merge);
                    }
                }
            }
        }
        
}


  
        \DB::beginTransaction();
        try {
            if($request->approved_at && $request->approved_time){
                $request['approved_at'] = $request->approved_at." ".$request->approved_time;
            }else{
                $request['approved_at'] = date('Y-m-d H:i:s');
            }
            $request['updated_at'] = date("Y-m-d h:i:s");
            unset($request['approved_time']);
            unset($request['id']);
            unset($request['picture_remove']);
            //$request['picture'] = !empty($request['picture_current'])?json_encode($request['picture_current']):NULL;
            $request['picture'] = !empty($request['picture_current'])?json_encode($request['picture_current']):NULL;
            //print_r($request->all());
            unset($request['picture_current']);
            if($request->hasFile('file')){
                if(Employeeleave::where('id',$id)->update($request->except('file'))){
                    \DB::commit();
                    foreach( $picture_remove as $key => $value ) {
                        Storage::disk('leave_image')->delete( $value );
                    }
                    return "คุณอัพเดทข้อมูลสำเร็จ!";
                }else{
                    // throw new Exception("Error Processing Request", $e);
                }
            }else{
                if(Employeeleave::where('id',$id)->update($request->all())){
                    \DB::commit();
                
                    

                    foreach( $picture_remove as $key => $value ) {
                        Storage::disk('leave_image')->delete( $value );
                    }
                    return "คุณอัพเดทข้อมูลสำเร็จ!";
                }else{
                    // throw new Exception("Error Processing Request", $e);
                }
            }
        } catch (Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $example = Employeeleave::findOrFail($id);
        try {
            if($example->delete()){
                \DB::commit();
                return "คุณลบข้อมูลสำเร็จ!";
            }else{
                throw new Exception('Error! Processing', 1);
            }
        } catch (Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }
}