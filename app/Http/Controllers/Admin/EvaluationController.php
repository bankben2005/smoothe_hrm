<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Evaluation;

class EvaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['menu'] = 'การประเมินผล';
        return view('admin.evaluation')->with($data);
    }

    public function list()
    {
        $model = Evaluation::query();
        $model->select(['evaluation.*','evaluation.id as evaluationid']);
        return  \DataTables::eloquent($model)
                ->addColumn('action', function ($rec) {
                    $str = '
                        <a class="btn btn-xs btn-warning btn-edit" href="#" data-id="'.$rec->evaluationid.'">
                            <i class="fa fa-edit"></i>
                        </a>

                        <a class="btn btn-xs btn-info btn-structure" href="#" data-id="'.$rec->evaluationid.'">
                            <i class="fa fa-project-diagram"></i>
                        </a>

                        <a class="btn btn-xs btn-success btn-save-structure" href="#" data-id="'.$rec->evaluationid.'" data-toggle="tooltip" data-placement="top" title="บันทึกโครงสร้างองค์กร">
                            <i class="fa fa-project-diagram"></i>
                        </a>

                        <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->evaluationid.'">
                            <i class="fa fa-trash"></i>
                        </a>
                    ';
                    return $str;
                })
                ->editColumn('status', function ($rec) {
                    return ($rec->status=='T') ? '<span class="badge badge-success">เปิด</span>' : '<span class="badge badge-danger">ปิด</span>';
                })
                ->addIndexColumn()
                ->rawColumns(['action','status'])
                ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (empty($request->id)) {
            $request['created_at'] = date("Y-m-d h:i:s");
            unset($request['id']);
            \DB::beginTransaction();
            try {
                if ($request->status=='T') {
                    Evaluation::where('status', 'T')->update(['status'=>'F']);
                }
                if ($result = Evaluation::insert($request->all())) {
                    \DB::commit();
                    return "คุณเพิ่มข้อมูลสำเร็จ!";
                } else {
                    throw new \Exception('Error! Processing', 1);
                }
            } catch (\Exception $e) {
                \DB::rollBack();
                return $e;
            }
        } else {
            return $this->update($request, $request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if ($result = Evaluation::find($id)) {
                return $result;
            } else {
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        unset($request['id']);
        \DB::beginTransaction();
        try {
            if ($request->status=='T') {
                Evaluation::where('status', 'T')->update(['status'=>'F']);
            }
            unset($request['id']);
            if ($result = Evaluation::where('id', $id)->update($request->all())) {
                \DB::commit();
                return "คุณอัพเดทข้อมูลสำเร็จ!";
            } else {
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $example = Evaluation::findOrFail($id);
        try {
            if ($example->delete()) {
                \DB::commit();
                return "คุณลบข้อมูลสำเร็จ!";
            } else {
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }
}
