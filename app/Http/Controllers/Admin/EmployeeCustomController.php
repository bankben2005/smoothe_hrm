<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Employee;


class EmployeeCustomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data['menu'] = 'Employee';
        $data['menu'] = 'ข้อมูลพนักงาน';
        $data['provinces'] = \App\Models\Provinces::get();
        $data['departments'] = \App\Models\Department::active()->get();
        $data['levels'] = \App\Models\Level::active()->get();
        $data['adminmenus'] = \App\AdminMenu::get();
        return view('admin.form_personal_information')->with($data);
    }

    public function list(){
        $model = Employee::query();
        $model->leftJoin('admin_users','admin_users.id','Employee.created_by');
        $model->select(['Employee.*','Employee.id as Employeeid','admin_users.name as adminusersname','Employee.created_at as Employeecreatedat']);
        return  \DataTables::eloquent($model)
            ->addColumn('action',function($rec){
                $str = '
                <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    แก้ไข
                </button>
                <div class="dropdown-menu" style="width: 91.2833px; position: absolute; transform: translate3d(0px, -134px, 0px); top: 0px; left: 0px; will-change: transform;" x-placement="top-start">
                    <a class="btn btn-xs btn-warning btn-edit" href="#" data-id="'.$rec->Employeeid.'">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->Employeeid.'">
                        <i class="fa fa-trash"></i>
                    </a>
                </div>
                ';
                return $str;
            })
            ->editColumn('picture_profile',function($rec){
                return '<img class="cover-photo" style="height:75px;" src="'.asset("$rec->picture_profile").'">';
            })
            ->addIndexColumn()
            ->rawColumns(['action','picture_profile'])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty($request->id)){
            $request['created_at'] = date("Y-m-d h:i:s");
            unset($request['id']);
            \DB::beginTransaction();
            try {
                if($result = Employee::insert($request->all())){
                    \DB::commit();
                    return "คุณเพิ่มข้อมูลสำเร็จ!";
                }else{
                    throw new \Exception('Error! Processing', 1);
                }
            } catch (\Exception $e) {
                \DB::rollBack();
                return $e;
            }
        }else{
            return $this->update($request,$request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if($result = Employee::find($id)){
                return $result;
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        unset($request['id']);
        \DB::beginTransaction();
        try {
            unset($request['id']);
            if($result = Employee::where('id',$id)->update($request->all())){
                \DB::commit();
                return "คุณอัพเดทข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $example = Employee::findOrFail($id);
        try {
            if($example->delete()){
                \DB::commit();
                return "คุณลบข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }
}