<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Corporateinformation;


class CorporateinformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data['menu'] = 'corporateinformation';
        $data['menu'] = 'ข้อมูลองค์กร';
        return view('admin.corporateinformation')->with($data); // admin/corporateinformation
    }

    public function list(){
        $model = Corporateinformation::query();
        $model->leftJoin('admin_users','admin_users.id','corporateinformation.created_by');
        $model->leftJoin('employee','employee.id','admin_users.employee_id');
        $model->select(['corporateinformation.*','corporateinformation.id as corporateinformationid',\DB::raw('employee.firstname +\' \'+employee.lastname as employeename') ,
        'employee.lastname','corporateinformation.created_at as corporateinformationcreatedat']);
        return  \DataTables::eloquent($model)
            ->addColumn('action',function($rec){
                $str = '
                    <a class="btn btn-xs btn-warning btn-edit" href="#" data-id="'.$rec->corporateinformationid.'">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->corporateinformationid.'">
                        <i class="fa fa-trash"></i>
                    </a>
                ';
                return $str;
            })
            ->editColumn('picture',function($rec){
                return '<img class="cover-photo" style="height:75px;" src="'.asset("$rec->picture").'">';
            })
            ->addIndexColumn()
            ->rawColumns(['action','picture'])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty($request->id)){
            $request['created_at'] = date("Y-m-d h:i:s");
            unset($request['id']);
            \DB::beginTransaction();
            try {
                if($result = Corporateinformation::insert($request->all())){
                    \DB::commit();
                    return "คุณเพิ่มข้อมูลสำเร็จ!";
                }else{
                    throw new \Exception('Error! Processing', 1);
                }
            } catch (\Exception $e) {
                \DB::rollBack();
                return $e;
            }
        }else{
            return $this->update($request,$request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if($result = Corporateinformation::find($id)){
                return $result;
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        unset($request['id']);
        \DB::beginTransaction();
        try {
            unset($request['id']);
            if($result = Corporateinformation::where('id',$id)->update($request->all())){
                \DB::commit();
                return "คุณอัพเดทข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $example = Corporateinformation::findOrFail($id);
        try {
            if($example->delete()){
                \DB::commit();
                return "คุณลบข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }
}