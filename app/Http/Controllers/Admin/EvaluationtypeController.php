<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Evaluation;
use App\Models\Evaluationtype;
use App\Models\Structurepivot;
use App\Models\Evaluationresult;
use App\Http\Controllers\FunctionController as FC;

use stdClass;

class EvaluationtypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['menu'] = 'ประเภทการประเมินผล';
        return view('admin.evaluationtype')->with($data); // admin/evaluationtype
    }

    public function list()
    {
        $model = Evaluationtype::query();
        
        $model->select(['evaluation_type.*','evaluation_type.id as evaluation_typeid']);
        return  \DataTables::eloquent($model)
                ->addColumn('action', function ($rec) {
                    $str = '
                    <a class="btn btn-xs btn-warning btn-edit" href="#" data-id="'.$rec->evaluation_typeid.'">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->evaluation_typeid.'">
                            <i class="fa fa-trash"></i>
                        </a>
                    ';
                    return $str;
                })
                ->editColumn('status', function ($rec) {
                    if ($rec->status=='T') {
                        return '<span class="badge badge-success">ใช้งาน</span>';
                    } else {
                        return '<span class="badge badge-danger">ไม่ใช้งาน</span>';
                    }
                })
                ->addIndexColumn()
                ->rawColumns(['action','status'])
                ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (empty($request->id)) {
            $request['created_at'] = date("Y-m-d h:i:s");
            unset($request['id']);
            \DB::beginTransaction();
            try {
                if ($result = Evaluationtype::insert($request->all())) {
                    \DB::commit();
                    return "คุณเพิ่มข้อมูลสำเร็จ!";
                } else {
                    throw new \Exception('Error! Processing', 1);
                }
            } catch (\Exception $e) {
                \DB::rollBack();
                return $e;
            }
        } else {
            return EvaluationtypeController::update($request, $request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if ($result = Evaluationtype::find($id)) {
                return $result;
            } else {
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        unset($request['id']);
        \DB::beginTransaction();
        try {
            unset($request['id']);
            if ($result = Evaluationtype::where('id', $id)->update($request->all())) {
                \DB::commit();
                return "คุณอัพเดทข้อมูลสำเร็จ!";
            } else {
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $example = Evaluationtype::findOrFail($id);
        try {
            if ($example->delete()) {
                \DB::commit();
                return "คุณลบข้อมูลสำเร็จ!";
            } else {
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    public static function query_get_value($employee_id=null, $evaluation_id=null)
    {
        $add_field = '';
        $add_where = '';
        if (isset($evaluation_id)) {
            $table = 'structure_evaluation';
            $add_where  .= " AND er.evaluation_id = $evaluation_id";
        // $add_where  .= " AND sp.evaluation_id = $evaluation_id";
        } else {
            $table = 'structure_pivot';
        }


        // ฟิลด์ข้อมูล AVG การประเมิน
        $add_field .=
        'sp.box_id
        ,sp.box_top_id
        ,sp.employee_id
        ,( SELECT count(*) FROM '.$table.' spi WHERE spi.box_top_id = sp.box_id )as child';
        $evalaution_type = EvaluationType::active()->get();
        foreach ($evalaution_type as $key => $value) {
            $add_field .=
            ',ROUND( AVG( CASE WHEN (er.evaluation_type_id = '.$value->id.' AND er.score>0 ) THEN er.score ELSE NULL END ) , 2 ) AS '.(str_replace(' ', '_', strtolower(trim($value->name))));
        }
        // $add_field .= isset($evaluation_id) ? ',er.evaluation_id' : '';
        // จัดการเงื่อนไขการประเมิน
        
        // $add_where  .= ($evaluation_id!=null) ? "AND er.evaluation_id = $evaluation_id" : '';
        // $add_where  .= ($evaluation_id!=null) ? "AND sp.evaluation_id = $evaluation_id" : '';


        // query สำหรับดึงคะแนนเฉลี่ยนตามไตรสามาส
        $str_query =
        "
            SELECT
                $add_field
            FROM
                $table sp
                LEFT JOIN evaluation_result er ON er.employee_target_id = sp.employee_id
                LEFT JOIN employee e ON e.id = er.employee_target_id
            WHERE
                ( sp.box_id LIKE ( SELECT TOP 1 s.box_id FROM $table s WHERE s.employee_id = $employee_id )+'%' )
                $add_where
            GROUP BY
                sp.box_id,
                sp.box_top_id,
                sp.employee_id
        ";
        $result = \DB::select($str_query);

        $data = array();

        foreach ($evalaution_type as $k => $v) {
            $score = EvaluationtypeController::recursive_evaluation_score($result, $employee_id, (str_replace(' ', '_', strtolower(trim($v->name)))));
            $obj = new stdClass();
            $obj->id = (string) $v->id;
            $obj->name = $v->name;
            $obj->ratio = $v->ratio;
            $obj->score = FC::star($score);
            $obj->score_float = $score;
            $obj->grade = FC::grade($score);
            $data[] = $obj;
        }
        return $data;
    }

    public static function recursive_evaluation_score($obj, $employee_id=null, $evaluation_type_name)
    {
        $parameter = $evaluation_type_name;
        $items = $obj;
        $array = [];
        $check_top_box_id = null;

        foreach ($items as $key => $item) {
            if ($employee_id==$item->employee_id) {
                $check_top_box_id = $item->box_id;
                $array[] = (float)($item->{$parameter});
                unset($items[$key]);
                break;
            }
        }

        $array_in = array();
        foreach ($items as $key => $item) {
            if ($check_top_box_id==$item->box_top_id) {
                if ((int)($item->child)!=0) {
                    $fetchs = EvaluationtypeController::recursive_evaluation_score($items, (int)($item->employee_id), $parameter);
                    if (is_array($fetchs)) {
                        $array_in[] = (count(array_filter($fetchs))==0) ? array_sum(array_filter($fetchs)) : array_sum(array_filter($fetchs)) / count(array_filter($fetchs));
                    } else {
                        $array_in[] = $fetchs;
                    }
                } else {
                    $array_in[] = (float)($item->{$parameter});
                }
                unset($items[$key]);
            }
        }

        if (!empty($array_in)) {
            $array[] = (count(array_filter($array_in))==0) ? array_sum(array_filter($array_in)) : array_sum(array_filter($array_in)) / count(array_filter($array_in));
        }
        if (!empty($array)) {
            $return = (count(array_filter($array))==0) ? array_sum(array_filter($array)) : array_sum(array_filter($array)) / count(array_filter($array));
        }

        $return = isset($return) ? $return : 0 ;

        return $return;
    }

    // ฟังก์ชันดึงคะแนน เกรด
    public static function calculate_result($employee_id=null, $start=null, $end=null, $eid=null)
    {
        // ฟังก์ชันคำนวณคะแนนดิบ
        $result = EvaluationtypeController::decode_calculate($employee_id, $start, $end, $eid);
        // return $result = EvaluationtypeController::query_get_value($employee_id, $eid);

        // คะแนน แยกตามประเภทการประเมิน (KJA)
        $return['evaluation_type'] = $result;
        
        $score = EvaluationtypeController::decode_calculate_grade($result);
        
        //คะแนนดิบ
        $return['score'] = (string)$score['result'];

        // $return['rawScore'] = $result;
        // $return['ratio'] = $score['ratio'];
        // $return['value'] = $score['value'];
        $return['items'] = $score;

        // ตัดเกรดตามช่วงคะแนน
        $return['grade'] = (string)FC::grade($score['result']) ;
        return $return;
    }

    public static function decode_calculate($employee_id=null, $start=null, $end=null, $eid=null)
    {
        // query หาค่าเฉลี่ย , หาคนอยู่ด้านใต้ id นั้น ตามไตรมาสการประเมิน และจัดเป็นชุดข้อมูลเพื่อทำ recursive avg ต่อ
        $items = EvaluationtypeController::calculate($employee_id, $start, $end, $eid);
        $data = $items['data'];
        $evaluation_type = Evaluationtype::get();
        $evaluation_result = [];
        foreach ($evaluation_type as $key => $value) {
            // ฟังกชันเฉลี่ยคะแนนจริง recursive avg => เฉลี่ยนจากชั้นล่างขึ้นมาจนถึง employee_id นั้น
            $score = EvaluationtypeController::fetch_empid($data, $start, $end, $value->id);

            $obj = new stdClass();
            $obj->id = (string)$value->id;
            $obj->name = (string)$value->name;
            $obj->ratio = (string)$value->ratio;
            $obj->score = FC::star((float)$score);
            // $obj->score_int = (string)( ROUND($score) );
            $obj->score_float = (string)((float)$score);

            // นำคะแนนมาเทียบตามตาราง
            $obj->grade = FC::grade($score);
            $obj->fetch_empid=($score);
            // $obj->data = $data;
            $evaluation_result[] = $obj;
        }
        return $result = $evaluation_result;
    }

    // query หาค่าเฉลี่ย , หาคนอยู่ด้านใต้ id นั้น ตามไตรมาสการประเมิน และจัดเป็นชุดข้อมูลเพื่อทำ recursive avg ต่อ
    public static function calculate($employee_id=null, $start=null, $end=null, $eid=null)
    {
        $time_start = microtime(true);
        $rows = Structurepivot::where('employee_id', $employee_id)
        ->leftjoin('employee', 'structure_pivot.employee_id', 'employee.id')
        ->select([
            'structure_pivot.*'
            ,\DB::raw('employee.firstname +\' \'+ employee.lastname as name')
        ])
        ->get();
        $return = [];
        foreach ($rows as $key => $row) {
            $obj = new \stdClass();
            $obj->employee_id = $row->employee_id;
            $obj->name = $row->name;
            $obj->box_id = $row->box_id;
            $obj->box_top_id = $row->box_top_id;

            // query หาค่าเฉลี่ย ตามไตรมาสการประเมิน
            // $obj->avg = EvaluationtypeController::avg((int)($row->employee_id), $start, $end, $eid);
            $obj->avg = EvaluationtypeController::avg($row->employee_id, $start, $end, $eid);
            // หาคนอยู่ด้านใต้ id นั้น
            $obj->child = EvaluationtypeController::get_child($row->box_id, $start, $end);
            $return[] = $obj;
        }
        $time_end = microtime(true);
        $excutetime = $time_end - $time_start;
        $data['data'] = $return;
        $data['excutetime'] = $excutetime;
        return $data;
    }

    public static function decode_calculate_grade($raw_score)
    {
        $ratio = 0;
        foreach ($raw_score as $key => $value) {
            $ratio += (int)$value->ratio;
        }
        $values = [];
        foreach ($raw_score as $key => $value) {
            // ขั้นตอนการเอา JKA มาคำณวนตาม ratio
            $values[] = ($value->score_float!=0) ? (($value->score_float * $value->ratio) / $ratio) : 0;
        }
        $result = array_sum($values);
        $data['ratio'] = $ratio;
        $data['value'] = $values;
        $data['result'] = $result;
        return $data;
    }

    // ฟังกชันเฉลี่ยคะแนนจริง recursive avg => เฉลี่ยนจากชั้นล่างขึ้นมาจนถึง employee_id นั้น
    public static function fetch_empid($obj, $start, $end, $evaluation_type_id)
    {
        $items = $obj;
        $array = [];
        foreach ($items as $key => $item) {
            foreach ($item->avg as $key => $value) {
                $value['score'] = (float)$value['score'];
                if ($value['id']==$evaluation_type_id) {
                    $array[] = $value['score'];
                }
            }
            if (count($item->child)!=0) {
                $fetchs = EvaluationtypeController::fetch_empid($item->child, $start, $end, $evaluation_type_id);
                if (is_array($fetchs)) {
                    // $array[] = ( count(array_filter($fetchs))==0 ) ? array_sum($fetchs) : array_sum($fetchs) / count( array_filter($fetchs) );
                    $array[] = (count(array_filter($array))==0) ? array_sum(array_filter($array)) : array_sum(array_filter($array)) / count(array_filter($array));
                } else {
                    $array[] = $fetchs;
                }
            }
        }
        $return = (count(array_filter($array))==0) ? array_sum(array_filter($array)) : array_sum(array_filter($array)) / count(array_filter($array));
        return $return;
    }

    public static function evaluation_score($allemployee=[], $start=null, $end=null)
    {
        return $evaluation_score = Evaluationtype::select([
            'evaluation_type.name'
            ,\DB::RAW(' AVG(evaluation_result.score) as score ')
            ,('evaluation_type.ratio as ratio')
        ])
        ->leftjoin('evaluation_result', function ($join) use ($allemployee,$start,$end) {
            $join->on('evaluation_result.evaluation_type_id', 'evaluation_type.id');
            $join->whereIn('evaluation_result.employee_target_id', $allemployee);
            if ($start!=null && $end!=null) {
                $join->whereBetween(
                    'evaluation_result.created_at',
                    [
                        \DB::raw('CONVERT(datetime,\''.$start.' 00:00:00\')')
                        ,\DB::raw('CONVERT(datetime,\''.$end.' 23:59:59\')')
                    ]
                );
            }
        })
        ->groupBy(['evaluation_type.id','evaluation_type.name','evaluation_type.ratio'])
        ->get();
    }

    public static function get_child($box_top_id, $start=null, $end=null)
    {
        $return = [];
        $rows = Structurepivot::where('box_top_id', $box_top_id)
        ->leftjoin('employee', 'structure_pivot.employee_id', 'employee.id')
        ->select([
            'structure_pivot.employee_id'
            ,\DB::raw('employee.firstname +\' \'+ employee.lastname as name')
            ,'structure_pivot.box_id'
            ,'structure_pivot.box_top_id'
        ])
        ->get();
        if ($rows) {
            foreach ($rows as $key => $row) {
                $obj = new \stdClass();
                $obj->employee_id = $row->employee_id;
                $obj->name = $row->name;
                $obj->box_id = $row->box_id;
                $obj->box_top_id = $row->box_top_id;
                $obj->avg = EvaluationtypeController::avg((int)($row->employee_id), $start, $end);
                $obj->child = EvaluationtypeController::get_child($row->box_id);
                $obj->child_count = count($obj->child);
                $return[] = $obj;
            }
        }
        return $return;
    }

    public static function avg($employee=null, $start_date=null, $end_date=null, $eid=null)
    {
        //query รายคน
        return $evaluation_score = Evaluationtype::select([
            'evaluation_type.id'
            ,'evaluation_type.name'
            ,\DB::RAW('
                AVG(evaluation_result.score)
                    as score
            ')
            // ,\DB::RAW('
            //     AVG(CASE
            //         WHEN ( evaluation_result.score>0 ) THEN evaluation_result.score
            //         ELSE NULL
            //     END)
            //         as score
            // ')
            ,('evaluation_type.ratio as ratio')
        ])
        ->leftjoin('evaluation_result', function ($join) use ($employee,$start_date,$end_date) {
            $join->on('evaluation_result.evaluation_type_id', 'evaluation_type.id');
            $join->where('evaluation_result.employee_target_id', $employee);
            if (isset($eid)) {
                $join->where('evaluation_result.evaluation_id', $eid);
            } else {
                if ($start_date!=null && $end_date!=null) {
                    $join->whereBetween(
                        'evaluation_result.created_at',
                        [
                            \DB::raw('CONVERT(datetime,\''.$start_date.' 00:00:00\')')
                            ,\DB::raw('CONVERT(datetime,\''.$end_date.' 23:59:59\')')
                        ]
                    );
                }
            }
        })
        ->groupBy(['evaluation_type.id','evaluation_type.name','evaluation_type.ratio'])
        ->get();
    }
}
