<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Evaluationresult;
use App\Models\Employee;
use App\Models\Evaluationtype;
use App\Models\Evaluation;
use App\Models\Groups;
use App\Models\Department;
use Excel;

use App\Exports\EvaluationresultExport;

class EvaluationresultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['employee'] = Employee::get();
        $data['evaluationtype'] = Evaluationtype::active()->get();
        $data['evaluations'] = Evaluation::get();
        $data['menu'] = 'ประวัติการประเมินผล';
        return view('admin.evaluationresult')->with($data); // admin/evaluationresult
    }

    public function list(Request $request)
    {
        $model = $this->report_query($request);
        return  \DataTables::eloquent($model)
                ->addColumn('action', function ($rec) {
                    $str = '';
                    // $str = '
                    // <a class="btn btn-xs btn-warning btn-edit" href="#" data-id="'.$rec->evaluation_resultid.'">
                    //     <i class="fa fa-edit"></i>
                    // </a>
                    // ';
                    $str .= '
                        <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->evaluation_resultid.'">
                            <i class="fa fa-trash"></i>
                        </a>
                    ';
                    return $str;
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->toJson();
    }

    public function export_evaluationresult(Request $request)
    {
        $data['evaluation_result'] = $result->get();
        // return Excel::download(new EvaluationresultExport($data), 'Evaluationresult.xlsx');
        // return Excel::create('ประวัติการประเมิน'.time(), function($excel) use ($data) {
        //     $excel->sheet('ประวัติการประเมิน', function($sheet) use ($data) {
        //         $sheet->loadView('admin.report.export.export_evaluationresult', $data);
        //     });
        // })->download('xls');
    }

    public function report_query(Request $request)
    {
        $model = Evaluationresult::query();
        $model->leftjoin(\DB::raw('employee e1'), 'evaluation_result.employee_id', 'e1.id');
        $model->leftjoin(\DB::raw('employee e2'), 'evaluation_result.employee_target_id', 'e2.id');
        $model->leftjoin('evaluation_type', 'evaluation_result.evaluation_type_id', 'evaluation_type.id');
        $model->leftjoin('evaluation', 'evaluation_result.evaluation_id', 'evaluation.id');
        $model->select([
                'e1.firstname as e1firstname'
                ,'e1.lastname as e1lastname'
                ,\DB::raw('e1.firstname +\' \'+ e1.lastname as e1name')
                ,'e2.firstname as e2firstname'
                ,'e2.lastname as e2lastname'
                ,\DB::raw('e2.firstname +\' \'+ e2.lastname as e2name')
                ,'evaluation_type.name as etname'
                ,'evaluation_type.id as evaluation_typeid'
                ,'evaluation_result.*'
                ,'evaluation_result.id as evaluation_resultid'
                ,'evaluation.evaluation_name as ename'
            ]);
        if (isset($request->evaluation_id)) {
            $model->where('evaluation_id', $request->evaluation_id);
        }
        if (isset($request->employee_id)) {
            $model->where('evaluation_result.employee_id', $request->employee_id);
        }
        if (isset($request->employee_target_id)) {
            $model->where('evaluation_result.employee_target_id', $request->employee_target_id);
        }
        return $model;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (empty($request->id)) {
            $request['created_at'] = date("Y-m-d h:i:s");
            unset($request['id']);
            \DB::beginTransaction();
            try {
                if ($result = Evaluationresult::insert($request->all())) {
                    \DB::commit();
                    return "คุณเพิ่มข้อมูลสำเร็จ!";
                } else {
                    throw new \Exception('Error! Processing', 1);
                }
            } catch (\Exception $e) {
                \DB::rollBack();
                return $e;
            }
        } else {
            return $this->update($request, $request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if ($result = Evaluationresult::find($id)) {
                return $result;
            } else {
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        unset($request['id']);
        \DB::beginTransaction();
        try {
            if ($result = Evaluationresult::where('id', $id)->update($request->all())) {
                \DB::commit();
                return "คุณอัพเดทข้อมูลสำเร็จ!";
            } else {
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $example = Evaluationresult::findOrFail($id);
        try {
            if ($example->delete()) {
                \DB::commit();
                return "คุณลบข้อมูลสำเร็จ!";
            } else {
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }
}