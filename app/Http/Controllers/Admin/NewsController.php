<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\News;

use App\Http\Controllers\API\NotificationController as Noti;
use App\Http\Controllers\API\APIController as API;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data['menu'] = 'News';
        $data['menu'] = 'ข้อมูลข่าวสาร';
        return view('admin.news')->with($data); // admin/news
    }

    public function list(){
        $model = News::query();
        $model->leftJoin('admin_users','admin_users.id','news.created_by');
        $model->leftJoin('employee','employee.id','admin_users.employee_id');
        $model->select(['news.*','news.id as newsid',\DB::raw('employee.firstname +\' \'+employee.lastname as employeename') ,
        'employee.lastname','news.created_at as newscreatedat']);
        return  \DataTables::eloquent($model)
            ->addColumn('action',function($rec){
                $str = '
                    <a class="btn btn-xs btn-warning btn-edit" href="#" data-id="'.$rec->newsid.'">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->newsid.'">
                        <i class="fa fa-trash"></i>
                    </a>
                ';
                return $str;
            })
            ->editColumn('picture',function($rec){
                return '<img class="cover-photo" style="height:75px;" src="'.asset("$rec->picture").'">';
            })
            ->addIndexColumn()
            ->rawColumns(['action','picture'])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty($request->id)){
            $request['created_at'] = date("Y-m-d h:i:s");
            unset($request['id']);
            \DB::beginTransaction();
            try {
                if($result = News::insertGetId($request->all())){
                    $params = new Request;
                    $params['segment'] = 'All';
                    $params['type'] = 'news_created';
                    $params['content'] = $request->name;
                    $data['news_id']=$result;
                    $data['type']=$params['type'];
                    $params['data'] = $data;
                    Noti::pushNotificationAll($params);
                    \DB::commit();
                    return "คุณเพิ่มข้อมูลสำเร็จ!";
                }
            }catch (\Exception $e) {
                \DB::rollBack();
                return $e->getMessage();
            }
        }else{
            return $this->update($request,$request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if($result = News::find($id)){
                return $result;
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        unset($request['id']);
        \DB::beginTransaction();
        try{
            unset($request['id']);
            if($result = News::where('id',$id)->update($request->all())){
                \DB::commit();
                return "คุณอัพเดทข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        }catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $example = News::findOrFail($id);
        try {
            if($example->delete()){
                \DB::commit();
                return "คุณลบข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }
}