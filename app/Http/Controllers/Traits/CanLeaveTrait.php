<?php

namespace App\Http\Controllers\Traits;

use App\Enums\LeaveResultStatus;
use App\Http\Controllers\FunctionController;
use App\Models\Employeeconfirm;
use App\Models\Employeeleave;
use App\Models\Leavetype;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

trait CanLeaveTrait
{
    /**
     * @param Request $request
     * @throws Exception
     */
    private function canLeave(Request $request): void
    {
        if (!$request->has('start_date') || !$request->has('end_date')) {
            return;
        }

        //echo $request->has('end_date');exit();

        if ($request->has('employee_id')) {
            $getLeaveType = $this->getEmployeeLeave(
                $request->get('employee_id'),
                $request->get('leave_type_id')
            );

            if ($request->has('id')) {
                $getLeaveType->where('id', '!=', $request->get('id'));
            }
            $employeeLeaves = $getLeaveType
                ->where(
                    'start_date',
                    '>=',
                    Carbon::make($request->get('start_date'))->startOfYear()->format(Carbon::DEFAULT_TO_STRING_FORMAT))
                ->where(
                    'start_date',
                    '<=',
                    Carbon::make($request->get('start_date'))->endOfYear()->format(Carbon::DEFAULT_TO_STRING_FORMAT))
                ->where('leave_result' , '=' , 'T')
                ->get();
           // ->where('leave_result' , '!=' , 'N')
            /** ======================
             * EDIT : 2021 - 02 -23
             * =======================
             */
            $durationLeave = FunctionController::leave_diff(
                $request->get('start_date'),
                $request->get('end_date'),
                $request->get('employee_id')
            );
            //echo  Carbon::make($request->get('start_date'))->startOfYear()->format(Carbon::DEFAULT_TO_STRING_FORMAT);exit();
            //echo '||';
            //echo  Carbon::make($request->get('start_date'))->endOfYear()->format(Carbon::DEFAULT_TO_STRING_FORMAT);exit();
            $days = 0;
            $hours = 0;
            list($durationLeave, $days) = $this->getLeaveDays($durationLeave, $days);
            $hours = $this->getLeaveHours($durationLeave, $hours);
            //echo '<PRE>';
            //print_r($hours);exit();
            foreach ($employeeLeaves as $employeeLeave) {
                $diffTime = FunctionController::leave_diff(
                    $employeeLeave->start_date,
                    $employeeLeave->end_date,
                    $employeeLeave->employee_id
                );

                list($diffTime, $days) = $this->getLeaveDays($diffTime, $days);

                $hours = $this->getLeaveHours($diffTime, $hours);
            }
            //echo $hours;exit();
            // print_r($days);exit();
            $allLeaveTimes = (($days * 8.50) + $hours) / 8.50;
            $leaveType = $this->getLeaveTypeById($request->get('leave_type_id'));
            
            if ($allLeaveTimes > $leaveType->amount) {
                throw new Exception('การลารวมของคุณเกินกำหนด', 422);
            }
        }
    }

    /**
     * @param int $employeeId
     * @param int|null $leaveTypeId
     * @return Builder
     */
    private function getEmployeeLeave(int $employeeId, int $leaveTypeId = null): Builder
    {
        //echo LeaveResultStatus::NOT_APPROVE;exit();
        return Employeeleave::query()
            ->select(['start_date', 'end_date'])
            ->where('leave_result', '!=', LeaveResultStatus::NOT_APPROVE)
            ->where('leave_type_id', $leaveTypeId)
            ->where('employee_id', $employeeId);
    }

    /**
     * @param string $diffTime
     * @param int $days
     * @return array
     */
    private function getLeaveDays(string $diffTime, int $days): array
    {
        if (strpos($diffTime, '(ว)')) {
            $explodeDays = explode('(ว)', $diffTime);
            $diffTime = last($explodeDays);
            $days += (int)head($explodeDays);
        }
        return array($diffTime, $days);
    }

    /**
     * @param $diffTime
     * @param int $hours
     * @return int
     */
    private function getLeaveHours($diffTime, int $hours): int
    {
        if (strpos($diffTime, '(ช)')) {
            $explodeHours = explode('(ช)', $diffTime);

            $hours += (int)head($explodeHours);
        }
        return $hours;
    }

    /**
     * @param int $leaveTypeId
     * @return Model
     */
    private function getLeaveTypeById(int $leaveTypeId): Model
    {
        return Leavetype::query()
            ->select('amount')
            ->find($leaveTypeId);
    }

    /**
     * @param Request $request
     * @throws Exception
     */
    private function checkLeaveInWorkingTime(Request $request): void
    {
        if (!$request->has('start_date') || !$request->has('end_date')) {
            return;
        }

        $date = Carbon::now();
        $getLeaveTime = FunctionController::get_leave_time($request->get('employee_id'));
        //print_r($getLeaveTime);exit();
        $workingTimeStart = Carbon::createFromFormat('H:i:s', data_get($getLeaveTime, 'start'))
            ->setDate($date->year, $date->month, $date->day);
        $workingTimeEnd = Carbon::createFromFormat('H:i:s', data_get($getLeaveTime, 'end'))
            ->setDate($date->year, $date->month, $date->day);
        $leaveTimeStart = Carbon::make($request->get('start_date'))
            ->setDate($date->year, $date->month, $date->day);
        $leaveTimeEnd = Carbon::make($request->get('start_date'))
            ->setDate($date->year, $date->month, $date->day);
        //echo $workingTimeStart.' || end:  '.$workingTimeEnd;exit();
        if (!$leaveTimeStart->between($workingTimeStart, $workingTimeEnd) || !$leaveTimeEnd->between(
                $workingTimeStart,
                $workingTimeEnd
            )) {
            throw new Exception('คุณไม่สามารถลานอกช่วงเวลางานได้', 422);
        }
    }

    /**
     * @param Request $request
     * @throws Exception
     */
    private function isEmployeeConfirmLeave(Request $request)
    {
        if (!$request->has('start_date') || !$request->has('end_date')) {
            return;
        }

        $startMonth = Carbon::make($request->get('start_date'));
        $endMonth = Carbon::make($request->get('end_date'));

        $employeeConfirm = Employeeconfirm::query()
            ->where('employee_id', $request->get('employee_id'))
            ->latest()
            ->first();

        if ($employeeConfirm) {
            $lastConfirmMonth = Carbon::make($employeeConfirm->month)->endOfMonth();

            if ($startMonth->isBefore($lastConfirmMonth) || $endMonth->isBefore($lastConfirmMonth)) {
                throw new Exception('ไม่สามารถลาย้อนหลังได้');
            }
        }
    }
}