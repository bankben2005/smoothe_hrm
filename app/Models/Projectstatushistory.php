<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Projectstatushistory extends Model
{
    protected $table = 'project_status_history';
    public $timestamps = true;
    public $fillable = ['status'];
}
