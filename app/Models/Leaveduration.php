<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Leaveduration extends Model
{
    protected $table = 'leave_duration';
    public $timestamps = true;
    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}