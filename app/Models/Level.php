<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table = 'level';
    public $timestamps = true;

    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}