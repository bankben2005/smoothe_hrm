<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employee';
    public $timestamps = true;

    public function scopeActive($query)
    {
        return $query->where('employee.resignation_date',NULL);
    }
}