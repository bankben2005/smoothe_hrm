<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hospitals extends Model
{
    protected $table = 'hospitals';
    public $timestamps = true;
    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}