<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employeeexperience extends Model
{
    protected $table = 'employee_experience';
    public $timestamps = true;
}