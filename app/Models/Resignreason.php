<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resignreason extends Model
{
    protected $table = 'resign_reason';
    public $timestamps = true;
    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}