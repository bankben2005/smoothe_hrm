<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Importevaluation extends Model
{
    protected $table = 'import_evaluation';
    protected $fillable = ['employee', 'employee_target', 'job', 'kpi', 'attitute' , 'updated_at' , 'created_at'];
    // protected $timestamps = true;
}