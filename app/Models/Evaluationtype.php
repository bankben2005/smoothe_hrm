<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evaluationtype extends Model
{
    protected $table = 'evaluation_type';
    public $timestamps = true;
    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}