<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employeeevaluationsetting extends Model
{
    protected $table = 'employee_evaluation_setting';
    public $timestamps = true;

    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}