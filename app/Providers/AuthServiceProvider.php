<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies(); 
        Passport::routes();
        //admin menu
        view()->composer('*', function($view){
            if(\Auth::guard('admin')->user()){
                $getam = json_decode(\Auth::guard('admin')->user()->access_menu);
                $getuse = \App\AdminMenu::whereIn('id',$getam)
                ->whereNotNull('main_menu')
                ->orderBy('sort')
                ->get();
                $view->with( 'adminmenu',$getuse );
            }
        });
    }
}
