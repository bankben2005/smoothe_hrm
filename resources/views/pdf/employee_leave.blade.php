<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>
    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }
        body {
            font-family: "THSarabunNew";
            font-size: 15px;
        }
        @page {
            size: 21cm 29.7cm;
            margin: 0.4in;
        }
        @media print {
            html,
            body {
                width: 210mm;
                height: 297mm;
                font-size : 16px;
            }
        }
        table {
            width: 100%;
        }
        table.datatable {
            text-align: center;
            padding:1px 0 1px 0;
            border: 1px solid #8a8a8a;
        }
        .datatable td , .datatable th {
            text-align: center;
            border: border: 1px solid #8a8a8a;
        }
        .datatable tr {
            line-height: 7.3px;
        }
        img.pictureprofile {
            width:80px;
            height:auto;
            border:solid 1px #8a8a8a;
            border-radius: 5px;
        }
    </style>
</head>
<body>
    <center>
        <h2>
            {{ $title }} 
        </h2>
    </center>
    <hr>
    <table>
        <tr>
            <td style="padding-right:10px; margin:5px;"><strong>รหัสพนักงาน</strong>&nbsp;{{ isset($data->employee_code)?$data->employee_code:'' }}</td>
            <td style="padding-right:10px; margin:5px;"><strong>ตำแหน่ง</strong>&nbsp;{{ isset($data->lname)?$data->lname:'' }}</td>
            <td rowspan="4">
                <img class="pictureprofile" src="{{public_path($data->picture_profile)}}"/>
            </td>
        </tr>
        <tr>
            <td style="padding-right:10px; margin:5px;"><strong>ชื่อ</strong>&nbsp;{{ isset($data->firstname)?$data->firstname:'' }}</td>
            <td style="padding-right:10px; margin:5px;"><strong>นามสกุล</strong>&nbsp;{{ isset($data->lastname)?$data->lastname:'' }}</td>
        </tr>
        <tr>
            <td style="padding-right:10px; margin:5px;"><strong>บริษัท</strong>&nbsp;{{ isset($data->cname)?$data->cname:'' }}</td>
            <td style="padding-right:10px; margin:5px;"><strong>สาขา</strong>&nbsp;{{ isset($data->bname)?$data->bname:'' }}</td>
        </tr>
        <tr>
            <td style="padding-right:10px; margin:5px;"><strong>ฝ่าย</strong>&nbsp;{{ isset($data->gname)?$data->gname:'' }}</td>
            <td style="padding-right:10px; margin:5px;"><strong>แผนก</strong>&nbsp;{{ isset($data->dname)?$data->dname:'' }}</td>
        </tr>
    </table>
    <hr>
    <table>
        <tr>
            <td style="padding-right:10px; margin:5px;"><strong>วันที่แจ้ง</strong>&nbsp;{{ isset($data->created_at)?$data->created_at:'' }}</td>
            <td style="padding-right:10px; margin:5px;"><strong>ประเภทการลา</strong>&nbsp;{{ isset($data->ltname)?$data->ltname:'' }}</td>
            <td style="padding-right:10px; margin:5px;"><strong>สถานะ</strong>&nbsp;{{ isset($data->leave_result)?$data->leave_result:'' }}</td>
        </tr>
        <tr>
            <td style="padding-right:10px; margin:5px;"><strong>วันที่เริ่มตั้น</strong>&nbsp;{{ isset($data->start_date)?$data->start_date:'' }}</td>
            <td style="padding-right:10px; margin:5px;"><strong>วันที่สิ้นสุด</strong>&nbsp;{{ isset($data->end_date)?$data->end_date:'' }}</td>
            <td style="padding-right:10px; margin:5px;"><strong>หมายเหตุ</strong>&nbsp;{{ isset($data->remark)?$data->remark:'' }}</td>
        </tr>
        <tr>
            <td style="padding-right:10px; margin:5px;"><strong>ผู้อนุมัติ</strong>&nbsp;{{ isset($data->apname)?$data->apname:'' }}</td>
            <td style="padding-right:10px; margin:5px;"><strong>อนุมัติวันวันที่</strong>&nbsp;{{ isset($data->approved_at)?$data->approved_at:'' }}</td>
            <td style="padding-right:10px; margin:5px;"><strong>ข้อมูลวันที่</strong>&nbsp;{{ isset($data_date)?$data_date:'' }}</td>
        </tr>
    </table>

</body>
</html>