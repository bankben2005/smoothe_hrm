@extends('admin.layouts.app')

@section('script')
<script src="{{asset('assets/admin/js/admin/workdaygroup.js')}}"></script>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5>
        <button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
            + {{ isset($menu) ? $menu : '' }}
        </button>
    </div>
    <div class="card-body">
        <table id="workdaygroup" class="table table-xs table-hover table-bordered table-striped dataTable no-footer" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ชื่อกลุ่ม</th>
                    {{-- <th>กลุ่มวันทำงาน</th> --}}
                    <th>วันที่</th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<form class="validateForm">
    <div class="modal fade slide-up disable-scroll" id="modalSlideUp" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="pg-close fs-14"></i>
                        </button>
                        <h5>{{ isset($menu) ? $menu : '' }}</h5>
                    </div>
                    <div class="modal-body">
                        <input class="form-control" type="hidden" name="id">
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label">ชื่อกลุ่ม</label>
                            <div class="col-sm-9">
                                <input type="text" name="name" placeholder="ชื่อกลุ่ม" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="meu_name" class="col-sm-3 col-form-label">กลุ่มวันทำงาน</label>
                            <div class="col-sm-9">
                                @foreach ($workday as $key => $item)
                                <div class="form-check">
                                    <input type="checkbox" value="{{$item->id}}" name="group_workday[]" id="{{$item->name_en}}">
                                    <label for="{{$item->name_en}}" class="form-check-label">{{$item->name_th}}{{!empty($item->detail)?"($item->detail)":''}}</label>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
                        <button type="submit" class="btn btn-success btn-cons">บันทึก</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@stop