@extends('admin.layouts.app')

@section('style')
<link rel="stylesheet" href="{{asset('assets/plugins/orgchart/css/jquery.orgchart.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/plugins/basicprimitives/primitives.latest.css')}}">
@stop

@section('script')
<script src="{{asset('assets/admin/js/admin/evaluation.js')}}"></script>

<script src="{{asset('assets/plugins/orgchart/js/jquery.orgchart.min.js')}}"></script>
<script src="{{asset('assets/plugins/basicprimitives/primitives.min.js')}}"></script>
<script src="{{asset('assets/plugins/basicprimitives/primitives.jquery.min.js')}}"></script>
{{-- <script>
    document.addEventListener('DOMContentLoaded', function () {
        $.ajax({
            type: "get",
            url: rurl + "admin/organizationalstructure/chart_json",
            dataType: "json",
            success: function (response) {
                var control;
                var options = new primitives.orgdiagram.Config();
                var items = response;
                options.items = items;
                options.pageFitMode = 1;
                options.textOrientationType = 0;
                options.childrenPlacementType = 3;
                options.navigationMode = 0;
                options.hasSelectorCheckbox = primitives.common.Enabled.False;
                $("#basicdiagram").orgDiagram(options);
            }
        });
    });
</script> --}}
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5>
        <button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
            + {{ isset($menu) ? $menu : '' }}
        </button>
    </div>
    <div class="card-body">
        <table id="evaluation" class="table table-xs table-hover table-bordered table-striped dataTable no-footer" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>รายการประเมินผล</th>
                    <th>รายละเอียด</th>
                    <th>เริ่มต้น</th>
                    <th>สิ้นสุด</th>
                    <th>สถานะ</th>
                    <th></th>
                </tr>
            </thead>
        </table>
        {{-- <div id="basicdiagram"></div> --}}
        <div id="basicdiagram" style="width: 100%; height: 640px"></div>
    </div>
</div>

<form class="validateForm">
    <div class="modal fade slide-up disable-scroll" id="modalSlideUp" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="pg-close fs-14"></i>
                        </button>
                        <h5>{{ isset($menu) ? $menu : '' }}</h5>
                    </div>
                    <div class="modal-body">
                        <input class="form-control" type="hidden" name="id">
                        <div class="form-group row">
                            <label for="evaluation_name" class="col-sm-3 col-form-label">รายการประเมินผล</label>
                            <div class="col-sm-9">
                                <input type="text" name="evaluation_name" placeholder="evaluation_name"
                                    class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="detail" class="col-sm-3 col-form-label">รายละเอียด</label>
                            <div class="col-sm-9">
                                <textarea name="detail" class="form-control input-sm"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="evaluation_start" class="col-sm-3 col-form-label">เริ่มต้น</label>
                            <div class="col-sm-9">
                                <input type="date" name="evaluation_start" placeholder="evaluation_start"
                                    class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="evaluation_end" class="col-sm-3 col-form-label">สิ้นสุด</label>
                            <div class="col-sm-9">
                                <input type="date" name="evaluation_end" placeholder="evaluation_end"
                                    class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="evaluation_end" class="col-sm-3 col-form-label">สถานะการเปิดประเมิน</label>
                            <div class="col-sm-9">
                                <select name="status" class="ls-select2">
                                    <option value="">== สถานะการประเมิน ==</option>
                                    <option value="T">เปิดการประเมิน</option>
                                    <option value="F">ปิดการประเมิน</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
                        <button type="submit" class="btn btn-success btn-cons">บันทึก</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="modal fade slide-up" id="modalChart" role="dialog">
    <div class="modal-dialog modal-full">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <h5 class="pull-left">แผนผัง{{ isset($menu) ? $menu : '' }}<span id="structure"></span></h5>
                    
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                        class="pg-close fs-14"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="chart_container" style="width: 1366px; height: 640px; border:solid 1px #888" />
                </div>
            </div>
        </div>
    </div>
</div>
@stop