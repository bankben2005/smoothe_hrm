<!-- BEGIN NAV -->
<nav class="page-sidebar" data-pages="sidebar">
    <div class="sidebar-overlay-slide from-top" id="appMenu"></div>
    <div class="sidebar-header">
        <img src="{{asset('assets/img/logo-hrm.png')}}" alt="logo" class="brand"
            data-src="{{asset('assets/img/logo-hrm.png')}}" data-src-retina="{{asset('assets/img/logo-hrm_2x.png')}}"
            width="78" height="22" />
    </div>
    <div class="sidebar-menu">
        <ul class="menu-items">
            @if(\Auth::guard('admin')->check())
                @php $current_url_id=null; @endphp
                @php
                foreach($adminmenu as $key => $value){
                    if( asset($value->menu_url) == url()->current() ){
                        $current_url_id = $value->main_menu;
                    }
                }
                @endphp
                @foreach($adminmenu as $key => $item)
                    @if($item->main_menu == 0)
                        <li class="{{((asset($item->menu_url)==url()->current())?'active':'')}} {{$current_url_id==$item->id?'active':''}} {{$item->id==1?'m-t-30':''}}">
                            <a href=" {{ !empty($item->menu_url)? asset($item->menu_url) : '#' }}">
                                <span class="title">{{$item->menu_name}}</span>
                                @if( ($item->main_menu==0) && empty($item->menu_url))
                                <span
                                    class="arrow {{ ( (url(''.$item->menu_url)==url()->current())||$current_url_id==$item->id ? 'open':'') }}"></span>
                                @endif
                            </a>
                            <span class="icon-thumbnail">
                                <i class="fa {!! $item->menu_icon !!}"></i>
                            </span>
                            @php
                            $check = null;
                            foreach($adminmenu as $key => $itemin){
                                if($item->id == $itemin->main_menu){
                                    $item ? $check[]=$itemin : '' ;
                                }
                            }
                            $check = $check==null ? 0 : count($check);
                            @endphp
                            @if($check!=0)<ul class="sub-menu">@endif
                            @foreach($adminmenu as $key => $itemin)
                                @if($item->id == $itemin->main_menu && $itemin->main_menu!=0)
                                <li class="{{ (( url( ''.$itemin->menu_url )==url()->current() ) ? 'open active':'') }}">
                                    <a href="{{asset($itemin->menu_url)}}">{{$itemin->menu_name}}</a>
                                    <span class="icon-thumbnail">
                                        <i class="fa {!! $itemin->menu_icon !!}"></i>
                                    </span>
                                </li>
                                @endif
                            @endforeach
                            @if($check!=0) </ul> @endif
                        </li>
                    @endif
                @endforeach
            @endif
        </ul>
        <div class="clearfix"></div>
    </div>
</nav>
<!-- END NAV -->