@extends('admin.layouts.app')

@section('style')
<link rel="stylesheet" href="{{asset('assets/plugins/jquery-datetimepicker/build/jquery.datetimepicker.min.css')}}">
@stop

@section('script')
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js "></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js "></script>
<script src="{{asset('assets/admin/js/admin/employeeleave_employee.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datetimepicker/build/jquery.datetimepicker.full.min.js')}}"></script>
<script>
	$('input[name="start_date"]').datetimepicker({
		format:'Y-m-d H:i',
  		lang:'th'
	});

	$('input[name="end_date"]').datetimepicker({
		format:'Y-m-d H:i',
  		lang:'th'
	});
</script>
@stop

@section('content')
<div class="card">
	<div class="card-header">
		<h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5> 
		<button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
			+ {{ isset($menu) ? $menu : '' }}
		</button>
	</div>
	<div class="card-body">
		<table id="employeeleave" class="table table-xs table-hover table-bordered table-striped dataTable no-footer" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>ประเภทการลา</th>
					<th>ช่วงเวลา</th>
					<th>ลาวันที่</th>
					<th>ถึงวันที่</th>
					<th>ชื่อ - นามสกุล</th>
					<th></th>
					<th>หมายเหตุ</th>
					<th>อนุมัติโดย</th>
					<th></th>
					<th>อนุมัติวันที่</th>
					<th>สถานะ</th>
					<!-- <th></th> -->
				</tr>
			</thead>
		</table>
	</div>
</div>

<form class="validateForm">
	<div class="modal fade slide-up" id="modalSlideUp" role="dialog" aria-hidden="false">
		<div class="modal-dialog modal-lg">
			<div class="modal-content-wrapper">
				<div class="modal-content">
					<div class="modal-header clearfix text-left">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
								class="pg-close fs-14"></i>
						</button>
						<h5>{{ isset($menu) ? $menu : '' }}</h5>
					</div>
					<div class="modal-body">
						<input class="form-control" type="hidden" name="id">
						<div class="form-group row">
							<label for="firstname" class="col-sm-3 col-form-label">ชื่อ - นามสกุล</label>
							<div class="col-sm-9">
								<select class="ls-select2" name="employee_id" id="employee_id" disabled>
									<option value="">== ชื่อ นามสกุล ==</option>
									@foreach ($employee as $key => $item)
									<option value="{{$item->id}}" {{ (\Auth::guard('admin')->user()->employee_id==$item->id?'selected':'') }}>{{$item->firstname}} {{$item->lastname}}</option>
									@endforeach
								</select>
								<input type="hidden" name="employee_id" value="{{(\Auth::guard('admin')->user()->employee_id)}}">
							</div>
						</div>
						<div class="form-group row">
							<label for="leave_name" class="col-sm-3 col-form-label">ประเภทการลา</label>
							<div class="col-sm-9">
								<select class="ls-select2" name="leave_type_id">
									<option value="">== ประเภทการลา ==</option>
									@foreach ($leavetype as $key => $item)
									<option value="{{$item->id}}">{{$item->leave_name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="duration_name" class="col-sm-3 col-form-label">ช่วงเวลา</label>
							<div class="col-sm-9">
								<select class="ls-select2" name="leave_duration_id">
									<option value="">== ช่วงเวลา ==</option>
									@foreach ($leaveduration as $key => $item)
									<option value="{{$item->id}}">{{$item->duration_name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="start_date" class="col-sm-3 col-form-label">ลาวันที่</label>
							<div class="col-sm-9">
								<input type="text" name="start_date" placeholder="start_date"
									class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="end_date" class="col-sm-3 col-form-label">ถึงวันที่</label>
							<div class="col-sm-9">
								<input type="text" name="end_date" placeholder="end_date" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="remark" class="col-sm-3 col-form-label">หมายเหตุ</label>
							<div class="col-sm-9">
								<textarea name="remark" class="form-control input-sm"></textarea>
							</div>
						</div>
						<input type="hidden" name="leave_result" value="N">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
						<button type="submit" class="btn btn-success btn-cons">บันทึก</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@stop