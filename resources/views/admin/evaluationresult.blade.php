@extends('admin.layouts.app')

@section('script')
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js "></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js "></script>
<script src="{{asset('assets/admin/js/admin/evaluationresult.js')}}"></script>
@stop

@section('content')
<div class="card">

	<div class="card-header">
		<div class="row">
			<div class="col-12">
				<h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5>
				<button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
					+ {{ isset($menu) ? $menu : '' }}
				</button>
			</div>
		</div>

		<form id="filter">
			<div class="row">
				<div class="col-md-2 col-sm-6">
					<select class="ls-select2" name="evaluation_id">
						<option value="">== การประเมิน ==</option>
						@foreach ($evaluations as $item)
							<option value="{{$item->id}}">{{$item->evaluation_name}}</option>
						@endforeach
					</select> 
				</div>
				<div class="col-md-2 col-sm-6">
					<select class="ls-select2" name="employee_id">
						<option value="">== ผู้ประเมิน ==</option>
						@foreach ($employee as $item)
							<option value="{{$item->id}}">{{$item->firstname .' '. $item->lastname}}</option>
						@endforeach
					</select> 
				</div>
				<div class="col-md-2 col-sm-6">
					<select class="ls-select2" name="employee_target_id">
						<option value="">== ผู้ถูกประเมิน ==</option>
						@foreach ($employee as $item)
							<option value="{{$item->id}}">{{$item->firstname .' '. $item->lastname}}</option>
						@endforeach
					</select> 
				</div>
				<div class="col-md-2 col-sm-6">
					<button class="btn btn-sm btn-primary btn-block" type="submit">ค้นหา</button>
				</div>
				{{-- <div class="col-md-2"></div> --}}
				{{-- <div class="col-md-2 col-sm-6">
					<button class="btn btn-sm btn-info btn-export"> <i class="fa fa fa-file-excel"></i>  นำออกข้อมูล</button>
				</div> --}}

				{{-- <div class="col-md-2 col-sm-6">
					<a class="btn btn-sm" href="#" target="blank">
						<i class="fa fa fa-file-excel"></i>  นำออกข้อมูลทั้งหมด</a>
				</div> --}}
			</div>
		</form>
	</div>
	<div class="card-body">
		<table id="evaluationresult" class="table table-xs table-hover table-bordered table-striped dataTable no-footer" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>การประเมิน</th>
					<th>ผู้ประเมิน</th>
					<th></th>
					<th></th>
					<th>ผู้ถูกประเมิน</th>
					<th></th>
					<th></th>
					<th>ประเภทการประเมิน</th>
					<th>คะแนน</th>
					<th>เหตุผล</th>
					<th>วันที่ประเมิน</th>
					<th></th>
				</tr>
			</thead>
		</table>
	</div>
</div>

{{-- <form class="validateForm">
	<div class="modal fade slide-up disable-scroll" id="modalSlideUp" role="dialog" aria-hidden="false">
		<div class="modal-dialog modal-lg">
			<div class="modal-content-wrapper">
				<div class="modal-content">
					<div class="modal-header clearfix text-left">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
								class="pg-close fs-14"></i>
						</button>
						<h5>{{ isset($menu) ? $menu : '' }}</h5>
					</div>
					<div class="modal-body">
						<input class="form-control" type="hidden" name="id">
						<div class="form-group row">
							<label for="firstname" class="col-sm-2 col-form-label">ผู้ประเมิน</label>
							<div class="col-sm-10">
								<select class="ls-select2" name="employee_id">
									<option value="">== ผู้ประเมิน ==</option>
									@foreach ($employee as $key => $item)
									<option value="{{$item->id}}">{{$item->firstname}} {{$item->lastname}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="firstname" class="col-sm-2 col-form-label">ผู้ถูกประเมิน</label>
							<div class="col-sm-10">
								<select class="ls-select2" name="employee_target_id">
									<option value="">== ผู้ถูกประเมิน ==</option>
									@foreach ($employee as $key => $item)
									<option value="{{$item->id}}">{{$item->firstname}} {{$item->lastname}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="name" class="col-sm-2 col-form-label">ประเภทการประเมิน</label>
							<div class="col-sm-10">
								<select class="ls-select2" name="evaluation_type_id">
									<option value="">== ประเภทการประเมิน ==</option>
									@foreach ($evaluationtype as $key => $item)
									<option value="{{$item->id}}">{{$item->name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="score" class="col-sm-2 col-form-label">คะแนน</label>
							<div class="col-sm-10">
								<input type="text" name="score" placeholder="score" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="reason" class="col-sm-2 col-form-label">เหตุผล</label>
							<div class="col-sm-10">
								<textarea name="reason" class="form-control input-sm"></textarea>
							</div>
						</div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
						<button type="submit" class="btn btn-success btn-cons">บันทึก</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form> --}}
@stop