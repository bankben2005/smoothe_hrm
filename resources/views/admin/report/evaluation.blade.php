@extends('admin.layouts.app')

@section('style')
<link href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" media="screen">
@endsection

@section('script')
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js "></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js "></script>
{{-- <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.th.js')}}"></script> --}}
<script>
    $('.ls-select2').select2();
    // var table = $('#datatable').DataTable({
    //     "responsive": true,
    //     "processing": true,
    //     "dom": "<'row' <'col-6'lB> <'col-6'f> >" + "rt" + "<'row' <'col-6' i><'col-6'p> >",
    //     "lengthMenu": [
    //         [10, 25, 50, -1],
    //         [10, 25, 50, "All"]
    //     ],
    //     buttons: [
    //         {
    //             extend: 'excel',
    //             className: 'btn btn-default btn-sm',
    //             text: '<i class="fas fa-file-excel"></i> Excel',
    //             title:"รายงานผลการประเมินคุณภาพรายบุคคล </br>"+$(".ls-select2 option:selected").text()+'</br> วันที่ '+$('[name="evaluation_start"]').val()+' ถึง '+$('[name="evaluation_end"]').val()
    //         },
    //         {
    //             extend: 'print',
    //             className: 'btn btn-default btn-sm',
    //             text: '<i class="fas fa-print"></i> Print',
    //             orientation: 'landscape',
    //             title:"รายงานผลการประเมินคุณภาพรายบุคคล </br>"+$(".ls-select2 option:selected").text()+'</br> วันที่ '+$('[name="evaluation_start"]').val()+' ถึง '+$('[name="evaluation_end"]').val()
    //         },
    //     ],
    // });

    $('#report').submit(function (e){
        e.preventDefault();
        swal({
            title: "กรุณารอ!",
            text: "ระบบกำลังทำการคำณวน",
            imageUrl: 'https://media0.giphy.com/media/sSgvbe1m3n93G/giphy.gif?cid=790b7611d61362dfe867287efcc467e76addd4b1219c4c50&rid=giphy.gif'
        });
        $.ajax({
            type: "POST",
            url: rurl + 'admin/report/evaluation/data',
            data: $(this).serialize(),
            dataType: "JSON",
            success: function (data) {
                swal("การคำนวณ สำเร็จ!", Math.round(data.excutetime) + ' วินาที', "success");
                var emp = data.employee;
                if (data.data.length != 0) {
                    // table.destroy();
                    $('tbody#evaluation').html('');
                    var table = $('#datatable').DataTable({
                        "responsive": true,
                        "processing": true,
                        "dom": "<'row' <'col-6'lB> <'col-6'f> >" + "rt" + "<'row' <'col-6' i><'col-6'p> >",
                        "lengthMenu": [
                            [10, 25, 50, -1],
                            [10, 25, 50, "All"]
                        ],
                        buttons: [
                            {
                                extend: 'excel',
                                className: 'btn btn-default btn-sm',
                                text: '<i class="fas fa-file-excel"></i> Excel',
                                title:"รายงานผลการประเมินคุณภาพรายบุคคล "+($(".ls-select2 option:selected").text())+" วันที่ "+($('[name="evaluation_start"]').val())+' ถึง '+($('[name="evaluation_end"]').val())
                            },
                            {
                                extend: 'print',
                                className: 'btn btn-default btn-sm',
                                text: '<i class="fas fa-print"></i> Print',
                                orientation: 'landscape',
                                title:"รายงานผลการประเมินคุณภาพรายบุคคล </br>"+$(".ls-select2 option:selected").text()+"</br> วันที่ "+$('[name="evaluation_start"]').val()+' ถึง '+$('[name="evaluation_end"]').val()
                            },
                        ],
                    });
                    table.clear().draw();
                    $.each(data.data, function (indexInArray, valueOfElement){
                        table.row.add([
                            (valueOfElement.employee_code!=null)?valueOfElement.employee_code:'',
                            (valueOfElement.name!=null)?valueOfElement.name:'',
                            (valueOfElement.employee_level_name!=null)?valueOfElement.employee_level_name:'',
                            (valueOfElement.group_name!=null)?valueOfElement.group_name:'',
                            (valueOfElement.department_name!=null)?valueOfElement.department_name:'',
                            (valueOfElement.position_name!=null)?valueOfElement.position_name:'',
                            (valueOfElement.branch_name!=null)?valueOfElement.branch_name:'',
                            (valueOfElement.leader_name!=null)?valueOfElement.leader_name:'',
                            (valueOfElement.j!=null)?valueOfElement.j:'',
                            (valueOfElement.k!=null)?valueOfElement.k:'',
                            (valueOfElement.a!=null)?valueOfElement.a:'',
                            (valueOfElement.score!=null)?valueOfElement.score:'',
                            (valueOfElement.grade!=null)?valueOfElement.grade:''
                        ]).draw();
                    });
                }
                $('.card.d-none').removeClass('d-none');
            },
            error: function (data) {}
        });
    });

    $('[name="evaluation_id"]').on('change', function (e) {
        $('[name="evaluation_start"]').val($(this).find(':selected').attr('data-start'));
        $('[name="evaluation_end"]').val($(this).find(':selected').attr('data-end'));
    });
</script>
@endsection

@section('content')
<!-- START card -->
<div class="card card-default">
    <div class="card-header ">
        <div class="card-title text-center">
            <h5>รายงานผลการประเมินคุณภาพรายบุคคล</h5>
        </div>
    </div>
    <div class="card-body">
        <form class="form" id="report" action="" method="post">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="form-input-group">
                        <label>ช่วงการประเมิน</label>
                        <select class="ls-select2" name="evaluation_id" required>
                            <option value="">== เลือกช่วงการประเมิน ==</option>
                            @foreach($evaluation as $key => $item)
                            <option value="{{$item->id}}" data-start="{{$item->evaluation_start}}" data-end="{{$item->evaluation_end}}" >
                                {{$item->evaluation_name}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="form-input-group">
                        <label>วันที่</label>
                        <input name="evaluation_start" type="text" class="form-control" id="datepicker-component1" required readonly>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="form-input-group">
                        <label>ถึง</label>
                        <input name="evaluation_end" type="text" class="form-control" id="datepicker-component2" required readonly>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="form-input-group">
                        <label></label>
                        <div class="clearfix" style="margin-top: 7px;"></div>
                        <button class="btn btn-block btn-primary" type="submit">ค้นหา</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="card card-default">
    <div class="card-body">
        <table class="table" id="datatable" style="width:100%">
            <thead>
                <tr>
                    <th width="30px;">รหัส</th>
                    <th>ชื่อ</th>
                    <th>ระดับ</th>
                    <th>ฝ่าย</th>
                    <th>แผนก</th>
                    <th>ตำแหน่ง</th>
                    <th>สาขา</th>
                    <th>หัวหน้า</th>
                    <th>J</th>
                    <th>K</th>
                    <th>A</th>
                    <th>Score</th>
                    <th>Grade</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<!-- END card -->
@endsection