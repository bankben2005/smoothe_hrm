@extends('admin.layouts.app')

@section('style')
<link href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" media="screen">
<link rel="stylesheet" href="{{asset('template/condensed/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}">
<style>
    table.dataTable thead > tr > th.sorting_asc, table.dataTable thead > tr > th.sorting_desc, table.dataTable thead > tr > th.sorting, table.dataTable thead > tr > td.sorting_asc, table.dataTable thead > tr > td.sorting_desc, table.dataTable thead > tr > td.sorting {
        padding: .75rem !important;
        background-image: none !important;
    }
</style>
@stop

@section('script')
<script src="{{asset('template/condensed/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.th.js')}}"></script>
<script src="{{asset('template/condensed/assets/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('template/condensed/assets/plugins/moment/moment-with-locales.min.js')}}"></script>
<script src="{{asset('template/condensed/assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script>
    $('.ls-select2').select2();
    $('.db').datepicker({
        format: 'yyyy-mm',
        autoclose: true,
        language: 'th',
        minViewMode: "months",
        showOtherMonths: true, 
        selectOtherMonths: true,
    })
    .on('changeDate', function() {
        get_data();
    });

    $('#daterangepicker').daterangepicker({
        timePicker: false,
        timePickerIncrement: 30,
        format: 'YYYY-MM-DD',
        setDate:"{{date('Y-m-d')}}"
    }, function(start, end, label) {
        $('[name="date_start"]').val(start.format('YYYY-MM-DD'));
        $('[name="date_end"]').val(end.format('YYYY-MM-DD'));
    });

    var get_data = function(){
        var dayname = ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'];
        var month = $('[name="month"]').val();
        var sp = month.split('-');
        $.ajax({
            type: "get",
            url: rurl+"admin/manage_shift/list",
            dataType:"json",
            data: { month : month },
            success: function (response) {
                $('#shift').find('thead').html('<tr><th>#</th><th width="250px">ชื่อ - นามสกุล</th></tr>');
                var str = '';
                for (let index = 1; index <= response.days; index++) {
                    var a = new Date(sp[0],sp[1],index); 
                    str += '<th>'+index+'<br/>'+dayname[a.getDay()]+'</th>';
                }
                $('#shift').find('thead').find('tr:first').append(str);
                $('#shift').find('tbody').html('');
                $.each(response.data, function (index, value){
                    $('#shift').find('tbody').append('<tr></tr>');
                    $('#shift').find('tbody').find('tr:last').append(
                        '<td>'+(index+1)+'</td><td>'+(value.name)+'</td>'
                    );
                    $.each(value.list, function (indexInArray, valueOfElement){
                        var str_append = '';
                        str_append = '<td class="text-center">';
                        if(valueOfElement.length==0){
                            str_append += '<p class="text-center">-</p>';
                        }else{
                            $.each(valueOfElement, function (ii, vv){
                                var value_sm_id = vv.id;
                                var value_shift_name = (vv.shift_name);
                                str_append += ii==0?'':'/';
                                str_append += '<a data-toggle="modal" data-target="#modalChanges" data-sm-id="'+value_sm_id+'" class="btn-change" href="#">'+value_shift_name+'</a>';
                            });
                        }
                        str_append += '</td>';
                        $('#shift').find('tbody').find('tr:last').append(str_append);
                    });
                });
                $("#shift").DataTable();
            }
        });
    }

    $("#shift").on("click" ,"td a.btn-change", function (e) {
        var sm_id = $(this).data('sm-id');
        $.ajax({
            type: "get",
            url: rurl+"admin/manage_shift/"+sm_id,
            success: function(response){
                $.each(response, function (indexInArray, valueOfElement) {
                    if(indexInArray=='time_start'||indexInArray=='time_end'){
                        if(valueOfElement!=null){
                            var time = valueOfElement.split(".");
                            $("#"+indexInArray).html(time[0])
                        }
                    }else{
                        $("#"+indexInArray).html(valueOfElement)
                    }
                    $("#modalChange").find('[name="'+indexInArray+'"]').val(valueOfElement);
                });
                $('.ls-select2').select2();
                $('#modalChange').modal('show');
                $('form.changeForm').removeAttr("data-sm-id");
                $('form.changeForm').data( "sm-id", sm_id );
            }
        });
    });

    $('a.delete-shift').click(function (e) { 
        e.preventDefault();
        var sm_id =$(this).closest('form').data('sm-id');
        swal({
            title: "คุณแน่ใจไหม?",
            text: "คุณจะไม่สามารถกู้คืนข้อมูลนี้ได้!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "ใช่ ลบทิ้ง!",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                type: "delete",
                url: rurl+"admin/manage_shift/"+sm_id,
                data: "data",
                success: function (response) {
                    $('[data-dismiss="modal"]').trigger('click');
                    swal('ลบรายการ', response.message, response.status);
                    get_data();
                }
            });
        });
    });

    $('.changeForm').submit(function (e) {
        e.preventDefault();
        // console.log($(this).serialize());
        var sm_id = $(this).data('sm-id');
        var btn = $('.validateForm [type="submit"]');
        btn.prop('disabled', true);
        $.ajax({
            type: "post",
            url: rurl+"admin/manage_shift/"+sm_id,
            data: $(this).serialize(),
            success: function(response){
                $('[data-dismiss="modal"]').trigger('click');
                btn.prop('disabled', false)
                swal('บันทึก', response.message, response.status);
                get_data();
            }
        });
    });

    $('.validateForm').submit(function (e){
        e.preventDefault();
        var btn = $('.validateForm [type="submit"]');
        btn.prop('disabled', true)
        $.ajax({
            type: "post",
            url: rurl+"admin/manage_shift",
            data: $(this).serialize(),
            success: function(response){
                $('[data-dismiss="modal"]').trigger('click');
                btn.prop('disabled', false)
                swal('บันทึก', response.message, response.status);
                get_data();
            }
        });
    });
    get_data();
</script>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-3">
                <input type="text" name="month" class="form-control db table-border" value="{{ date('Y-m') }}">
            </div>
            <div class="col-3">

            </div>
            <div class="col-3">

            </div>
            <div class="col-3">
                <button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
                    + {{ isset($menu) ? $menu : '' }}
                </button>
            </div>  
        </div>
        <br>
        <table id="shift" class="table table-responsive table-bordered table-striped" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th width="250px">ชื่อ - นามสกุล</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<form class="validateForm">
    <div class="modal fade slide-up disable-scroll" id="modalSlideUp" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="pg-close fs-14"></i>
                        </button>
                        <h5>{{ isset($menu) ? $menu : '' }}</h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="input_shift" class="col-sm-2 col-form-label">กะทำงาน</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="shift_id" id="input_shift" required>
                                    <option value="">== กะทำงาน ==</option>
                                    @foreach ($shift as $key => $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="employee_id" class="col-sm-2 col-form-label">พนักงาน</label>
                            <div class="col-sm-10">
                                <select class="ls-select2 form-control" name="employee_id" required>
                                    <option value="">== พนักงาน ==</option>
                                    @foreach ($employee as $key => $item)
                                    <option value="{{$item->id}}">{{$item->firstname .' '. $item->lastname}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="daterangepicker" class="col-sm-2 col-form-label">ช่วงวันที่</label>
                            <div class="col-sm-10">
                                <input type="text" name="daterange" id="daterangepicker" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="date_start" class="col-sm-2 col-form-label">เริ่มต้น</label>
                            <div class="col-sm-10">
                                <input type="text" name="date_start" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="date_end" class="col-sm-2 col-form-label">สิ้นสุด</label>
                            <div class="col-sm-10">
                                <input type="text" name="date_end" class="form-control" required>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
                        <button type="submit" class="btn btn-success btn-cons">บันทึก</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<form class="changeForm">
    <div class="modal fade slide-up disable-scroll" id="modalChange" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="pg-close fs-14"></i>
                        </button>
                        <h5>แก้ไข{{ isset($menu) ? $menu : '' }}</h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">วันที่</label>
                            <div class="col-sm-10">
                                <p id="date"></p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">กะทำงาน</label>
                            <div class="col-sm-10">
                                <p id="shift_name"></p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">ตั้งแต่เวลา</label>
                            <div class="col-sm-10">
                                <span id="time_start"></span> ถึง <span id="time_end"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="employee_id" class="col-sm-2 col-form-label">พนักงาน</label>
                            <div class="col-sm-10">
                                <select class="ls-select2 form-control" name="employee_id" required>
                                    <option value="">== พนักงาน ==</option>
                                    @foreach ($employee as $key => $item)
                                    <option value="{{$item->id}}">{{$item->firstname .' '. $item->lastname}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="employee_id" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-10">
                                <a href="#" class="delete-shift"><span class="label label-danger">x ลบรายการ</span></a>
                            </div>
                        </div>
                        
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
                        <button type="submit" class="btn btn-success btn-cons">บันทึก</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@stop