<?php $__env->startSection('script'); ?>
<script src="<?php echo e(asset('assets/admin/js/admin/holiday.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="card">
    <div class="card-header">
        <h5 class="pull-left"><?php echo e(isset($menu) ? $menu : ''); ?></h5>
        <button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
            + <?php echo e(isset($menu) ? $menu : ''); ?>

        </button>
    </div>
    <div class="card-body">
        <table id="holiday" class="table table-xs table-hover table-bordered table-striped dataTable no-footer" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>วันที่</th>
                    <th>ชื่อวัน</th>
                    <th>รายละเอียด</th>
                    <th>สถานะ</th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<form class="validateForm">
    <div class="modal fade slide-up disable-scroll" id="modalSlideUp" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="pg-close fs-14"></i>
                        </button>
                        <h5><?php echo e(isset($menu) ? $menu : ''); ?></h5>
                        
                    </div>
                    <div class="modal-body">
                        <input class="form-control" type="hidden" name="id">
                        <div class="form-group row">
                            <label for="holiday_date" class="col-sm-2 col-form-label">วันที่</label>
                            <div class="col-sm-10">
                                <input type="date" name="holiday_date" placeholder="วันที่"
                                    class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="holiday_name" class="col-sm-2 col-form-label">ชื่อวัน</label>
                            <div class="col-sm-10">
                                <input type="text" name="holiday_name" placeholder="ชื่อวัน"
                                    class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="detail" class="col-sm-2 col-form-label">รายละเอียด</label>
                            <div class="col-sm-10">
                                <textarea name="detail" class="form-control input-sm"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="in_out_distance" class="col-sm-2 col-form-label">การใช้งาน</label>
                            <div class="col-sm-10">
                                <select class="ls-select2" name="status">
                                    <option value="">== สถานะ ==</option>
                                    <option value="T">เปิด</option>
                                    <option value="F">ปิด</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
                        <button type="submit" class="btn btn-success btn-cons">บันทึก</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smoothe_hrm\resources\views/admin/holiday.blade.php ENDPATH**/ ?>