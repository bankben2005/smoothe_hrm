<?php $__env->startSection('style'); ?>
<link rel="stylesheet" href="<?php echo e(asset('assets/plugins/chartjs/Chart.min.css')); ?>">
<style>
    canvas {
        padding: 10px;
    }
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<?php
$label_gender = ""; $data_gender=""; $color_gender="";
foreach ($count_gender as $key => $value) {
    $label_gender .= "'".$value->gender."'".",";
    $data_gender .= $value->c.",";
    $color_gender .='randomRGB(),';
}
$label_company = ""; $data_company=""; $color_company="";
foreach($count_company as $key => $item){
    $label_company .= "'".$item->name."'".",";
    $data_company .= $item->c.",";
    $color_company .= 'randomRGB(),';
}
$label_group = ""; $data_group=""; $color_group="";
foreach($count_group as $key => $item){
    $label_group .= "'".$item->g."'".",";
    $data_group .= $item->c.",";
    $color_group .= 'randomRGB(),';
}
$label_department = ""; $data_department=""; $color_department="";
foreach($count_department as $key => $item){
    $label_department .= "'".$item->d."'".",";
    $data_department .= $item->c.",";
    $color_department .= 'randomRGB(),';
}
$label_level = ""; $data_level=""; $color_level="";
foreach($count_level as $key => $item){
    $label_level .= "'".$item->l."'".",";
    $data_level .= $item->c.",";
    $color_level .= 'randomRGB(),';
}
$label_employee_level = ""; $data_employee_level=""; $color_employee_level="";
foreach($count_employee_level as $key => $item){
    $label_employee_level .= "'".$item->el."'".",";
    $data_employee_level .= $item->c.",";
    $color_employee_level .= 'randomRGB(),';
}
$label_range = ""; $data_range=""; $color_range="";
foreach($count_range as $key => $item){
    $label_range .= "'".$item->range."'".",";
    $data_range .= $item->c.",";
    $color_range .= 'randomRGB(),';
}

$label_age = ""; $data_age=""; $color_age="";
foreach($count_age as $key => $item){
    $label_age .= "'".$item->range."'".",";
    $data_age .= $item->c.",";
    $color_age .= 'randomRGB(),';
}
?>
<script src="<?php echo e(asset('assets/plugins/chartjs/Chart.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/admin/charts.js')); ?>"></script>
<script>
    var randomScalingFactor = function () {
        return Math.round(Math.random() * 100);
    };

    var randomNum = function () {
        return Math.floor(Math.random() * 255);
    }

    var randomRGB = function () {
        var red = randomNum();
        var green = randomNum();
        var blue = randomNum();
        return "rgb(" + red + "," + green + "," + blue + "," + 0.8 + ")";
    }

    new Chart(document.getElementsByClassName('chart_gender'), {
        type: 'doughnut',
        data: {
            labels: [<?php echo $label_gender; ?>],
            datasets: [{
                data: [<?php echo $data_gender; ?>],
                backgroundColor: [<?php echo $color_gender; ?>],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: false,
            title: {
                display: true,
                text: 'จำนวนพนักงาน(เพศ)'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    });

    new Chart(document.getElementsByClassName('chart_company'), {
        type: 'doughnut',
        data: {
            labels: [<?php echo $label_company; ?>],
            datasets: [{
                data: [<?php echo $data_company; ?>],
                backgroundColor: [<?php echo $color_company; ?>],
                borderColor: [<?php echo $color_company; ?>],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: false,
            title: {
                display: true,
                text: 'จำนวนพนักงาน(บริษัท)'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    });

    new Chart(document.getElementsByClassName('chart_group'), {
        // type: 'horizontalBar',
        type: 'bar',
        data: {
            labels: [<?php echo $label_group; ?>],
            datasets: [{
                data: [<?php echo $data_group; ?>],
                backgroundColor: [<?php echo $color_group; ?>],
                borderColor: [<?php echo $color_group; ?>],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: false,
            title: {
                display: true,
                text: 'จำนวนพนักงาน(ฝ่าย)'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    });

    new Chart(document.getElementsByClassName('chart_employee_level'), {
        type: 'doughnut',
        data: {
            labels: [<?php echo $label_employee_level; ?>],
            datasets: [{
                data: [<?php echo $data_employee_level; ?>],
                backgroundColor: [<?php echo $color_employee_level; ?>],
                borderColor: [<?php echo $color_employee_level; ?>],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: false,
            title: {
                display: true,
                text: 'จำนวนพนักงาน(ระดับพนักงาน)'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    });

    new Chart(document.getElementsByClassName('chart_range'), {
        type: 'bar',
        data: {
            labels: [<?php echo $label_range; ?>],
            datasets: [{
                data: [<?php echo $data_range; ?>],
                backgroundColor: [<?php echo $color_range; ?>],
                borderColor: [<?php echo $color_range; ?>],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: false,
            title: {
                display: true,
                text: 'จำนวนพนักงาน(อายุงาน)'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    });

    new Chart(document.getElementsByClassName('chart_age'), {
        type: 'bar',
        data: {
            labels: [<?php echo $label_range; ?>],
            datasets: [{
                data: [<?php echo $data_range; ?>],
                backgroundColor: [<?php echo $color_range; ?>],
                borderColor: [<?php echo $color_range; ?>],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: false,
            title: {
                display: true,
                text: 'จำนวนพนักงาน(อายุ)'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    });
</script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-lg-6 col-xlg-6">
        <div class="row">
            <div class="col-md-12 m-b-10">
                <div class="ar-3-2 widget-1-wrapper">
                    <div class="card widget-loader-circle-lg">
                        <div class="card-header top-right">
                            <div class="card-controls">
                                <ul>
                                    <li><a data-toggle="refresh" class="card-refresh text-black" href="#"><i
                                        class="card-icon card-icon-refresh-lg-master"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="col-bottom widget-4-chart-container">
                                <div class="widget-4-chart rickshaw_graph">
                                    <canvas class="chart_group"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-xlg-6">
        <div class="row">
            <div class="col-sm-4 m-b-10">
                <div class="ar-1-1">
                    <div class="card">
                        <div class="card-body">
                            <div class="col-bottom widget-4-chart-container">
                                <div class="widget-4-chart rickshaw_graph">
                                    <canvas class="chart_gender" width="75" height="75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 m-b-10">
                <div class="ar-1-1">
                    <div class="widget-3 card widget-loader-bar">
                        <div class="card-body no-padding full-height">
                            <div class="widget-4-chart rickshaw_graph">
                                <canvas class="chart_company" width="75" height="75"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 m-b-10">
                <div class="ar-1-1">

                    <div class="card widget widget-loader-circle-lg no-margin">
                        <div class="card-body no-padding full-height">
                            <div class="widget-4-chart rickshaw_graph">
                                <canvas class="chart_employee_level" width="75" height="75"></canvas>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 m-b-10">
                <div class="ar-2-1">
                    <div class="card widget-loader-circle-lg">
                        <div class="col-bottom widget-4-chart-container">
                            <div class="widget-4-chart rickshaw_graph">
                                <canvas class="chart_range"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 m-b-10">
                <div class="ar-2-1">
                    <div class="card widget-loader-circle-lg">
                        <div class="col-bottom widget-4-chart-container">
                            <div class="widget-4-chart rickshaw_graph">
                                <canvas class="chart_age"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smoothe_hrm\resources\views/admin/chart.blade.php ENDPATH**/ ?>