<!-- START COPYRIGHT -->
<!-- START CONTAINER FLUID -->
<div class=" container-fluid  container-fixed-lg footer">
    <div class="copyright sm-text-center">
        <p class="small no-margin pull-right sm-pull-reset">
            <span class="hint-text">Copyright &copy; 2019 </span>
            <span class="font-montserrat">LASERPS</span>
        </p>
        <div class="clearfix"></div>
    </div>
</div>
<!-- END COPYRIGHT --><?php /**PATH C:\xampp\htdocs\smoothe_hrm\resources\views/admin/layouts/footer.blade.php ENDPATH**/ ?>