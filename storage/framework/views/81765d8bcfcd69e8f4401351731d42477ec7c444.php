  

<?php $__env->startSection('style'); ?>
<style>
    .table tbody tr td , .table tbody tr th {
        padding: .75rem !important;
    }
</style>
<link href="<?php echo e(asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css')); ?>" rel="stylesheet" type="text/css" media="screen">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js "></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js "></script>
<script src="<?php echo e(asset('assets/admin/js/admin/employeeregistration.js')); ?>"></script>
<script src="<?php echo e(asset('template/condensed/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.th.js')); ?>"></script>
<script>
	$('.db').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        language: 'th',
        showOtherMonths: true, 
        selectOtherMonths: true,
    })
</script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="card">
	<div class="card-header">
		<h5 class="pull-left"><?php echo e(isset($menu) ? $menu : ''); ?></h5>
		<button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
			+ <?php echo e(isset($menu) ? $menu : ''); ?>

		</button>
	</div>
	<div class="card-body">
		<form id="filter">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<select class="ls-select2" name="company_id">
						<option value="">== บริษัท ==</option>
						<?php $__currentLoopData = $company; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select> 
				</div>
				<div class="col-md-3 col-sm-6">
					<select class="ls-select2" name="branch_id">
						<option value="">== สาขา ==</option>
						<?php $__currentLoopData = $branch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<option value="<?php echo e($item->id); ?>"><?php echo e($item->branch_name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>
				</div>
				<div class="col-md-3 col-sm-6">
					<select class="ls-select2" name="group_id">
						<option value="">== ฝ่าย ==</option>
						<?php $__currentLoopData = $group; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select> 
				</div>
				<div class="col-md-3 col-sm-6">
					<select class="ls-select2" name="department_id">
						<option value="">== แผนก ==</option>
						<?php $__currentLoopData = $department; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select> 
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<select class="ls-select2" name="employee_id">
						<option value="">== พนักงาน ==</option>
						<?php $__currentLoopData = $employee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<option value="<?php echo e($item->id); ?>"><?php echo e($item->firstname); ?> <?php echo e($item->lastname); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select> 
				</div>
				<div class="col-md-3 col-sm-6">
					<input type="text" class="form-control db" name="in_date" placeholder="วันที่เข้างาน">
				</div>
				<div class="col-md-3 col-sm-6">
					<input type="text" class="form-control db" name="out_date" placeholder="วันที่ออกงาน">
				</div>
				<div class="col-md-3 col-sm-6">
					<button type="submit" class="btn btn-primary btn-block">ค้นหา</button>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<div class="table-responsive">
			<table id="employeeregistration" class="table table-xs table-hover table-bordered table-striped dataTable no-footer" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>#</th>
						<th>ชื่อ - นามสกุล</th>
						<th></th>
						<th>วันที่(เข้า)</th>
						<th>เวลา(เข้า)</th>
						<th>ละติจูด(เข้า)</th>
						<th>ลองติจูด(เข้า)</th>
						<th>เหตุผล(เข้า)</th>
						<th>วันที่(ออก)</th>
						<th>เวลา(ออก)</th>
						<th>ละติจูด(ออก)</th>
						<th>ลองติจูด(ออก)</th>
						<th>เหตุผล(ออก)</th>
						<th>created_at</th>
						<th></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

<form class="validateForm">
	<div class="modal fade slide-up" id="modalSlideUp" role="dialog" aria-hidden="false">
		<div class="modal-dialog modal-lg">
			<div class="modal-content-wrapper">
				<div class="modal-content">
					<div class="modal-header clearfix text-left">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
								class="pg-close fs-14"></i>
						</button>
						<h5><?php echo e(isset($menu) ? $menu : ''); ?></h5>
					</div>
					<div class="modal-body">
						<input class="form-control" type="hidden" name="id">
						<div class="form-group row">
							<label for="in_date" class="col-sm-3 col-form-label">วันที่(เข้า)</label>
							<div class="col-sm-9">
								<input type="date" name="in_date" placeholder="in_date" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="in_time" class="col-sm-3 col-form-label">เวลา(เข้า)</label>
							<div class="col-sm-9">
								<input type="time" name="in_time" placeholder="in_time" class="form-control input-sm">
							</div>
						</div>
						
						<div class="form-group row">
							<label for="in_reason" class="col-sm-3 col-form-label">เหตุผล(เข้า)</label>
							<div class="col-sm-9">
								<input type="text" name="in_reason" placeholder="in_reason"
									class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="out_date" class="col-sm-3 col-form-label">วันที่(ออก)</label>
							<div class="col-sm-9">
								<input type="date" name="out_date" placeholder="out_date" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="out_time" class="col-sm-3 col-form-label">เวลา(ออก)</label>
							<div class="col-sm-9">
								<input type="time" name="out_time" placeholder="out_time" class="form-control input-sm">
							</div>
						</div>
						
						<div class="form-group row">
							<label for="out_reason" class="col-sm-3 col-form-label">เหตุผล(ออก)</label>
							<div class="col-sm-9">
								<input type="text" name="out_reason" placeholder="out_reason"
									class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="firstname" class="col-sm-3 col-form-label">ชื่อ - นามสกุล</label>
							<div class="col-sm-9">
								<select class="ls-select2" name="employee_id">
									<option value="">== ชื่อ นามสกุล ==</option>
									<?php $__currentLoopData = $employee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($item->id); ?>"><?php echo e($item->firstname); ?> <?php echo e($item->lastname); ?></option>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
							</div>
						</div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
						<button type="submit" class="btn btn-success btn-cons">บันทึก</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smoothe_hrm\resources\views/admin/employeeregistration.blade.php ENDPATH**/ ?>