<?php $__env->startSection('script'); ?>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js "></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js "></script>

<script src="<?php echo e(asset('assets/admin/js/admin/employee.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/admin/custom.js')); ?>"></script>
<script>
	$(document).ready(function () {
		$(".ls-select2").select2();
	});
</script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="card">
	<div class="card-header">
		<h5 class="pull-left"><?php echo e(isset($menu) ? $menu : ''); ?></h5>
		<a type="button" class="btn btn-theme btn-add pull-right" href="<?php echo e(url('admin/employee/create')); ?>">
			+ <?php echo e(isset($menu) ? $menu : ''); ?>

		</a>

		<hr>
		<br />
		<h6>ตัวกรองข้อมูล</h6>
		<br>
		<form id="filter">
			<div class="row">
				
					<div class="col-md-3 col-sm-6">
					<label for="groups">บริษัท</label>
					<select class="form-control ls-select2" name="companies" id="companies" tabindex="-1"
						aria-hidden="true">
						<option value="">== บริษัท ==</option>
						<?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>
					</div>
					<div class="col-md-3 col-sm-6">
						<label for="groups">สาขา</label>
						<select class="form-control ls-select2" name="branches" id="branches" tabindex="-1" aria-hidden="true">
							<option value="">== สาขา ==</option>
							<?php $__currentLoopData = $branch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($value->id); ?>"><?php echo e($value->branch_name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
					</div>
					<div class="col-md-3 col-sm-6">
						<label for="groups">ฝ่าย</label>
						<select class="form-control ls-select2" name="groups" id="groups" tabindex="-1" aria-hidden="true">
							<option value="">== ฝ่าย ==</option>
							<?php $__currentLoopData = $groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
					</div>
					<div class="col-md-3 col-sm-6">
						<label for="departments">แผนก</label>
						<select class="form-control ls-select2" name="departments" id="departments" tabindex="-1"
							aria-hidden="true">
							<option value="">== แผนก ==</option>
							<?php $__currentLoopData = $department; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
					</div>
					<div class="col-md-3 col-sm-6">
						<label for="leves">ตำแหน่ง</label>
						<select class="form-control ls-select2" name="levels" id="levels" tabindex="-1" aria-hidden="true">
							<option value="">== ตำแหน่ง ==</option>
							<?php $__currentLoopData = $level; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
					</div>
					<div class="col-md-3 col-sm-6">
						<label for="leves">ระดับพนักงาน</label>
						<select class="form-control ls-select2" name="employee_level_id" id="employeelevel" tabindex="-1"
							aria-hidden="true">
							<option value="">== ระดับพนักงาน ==</option>
							<?php $__currentLoopData = $employee_level; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
					</div>
					<div class="col-md-3 col-sm-6">
						<label for="leves">ประกันสังคม</label>
						<select class="form-control ls-select2" name="hospital_id" id="hospital" tabindex="-1"
							aria-hidden="true">
							<option value="">== ประกันสังคม ==</option>
							<?php $__currentLoopData = $hospital; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
					</div>
					<div class="col-md-3 col-sm-6">
						<label for="leves">สถานะ</label>
						<select class="form-control ls-select2" name="employee_status_id" id="employeestatus" tabindex="-1"
							aria-hidden="true">
							<option value="">== สถานะ ==</option>
							<?php $__currentLoopData = $employeestatus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
					</div>
				

			</div>
		</form>
		<br>
	</div>
</div>

<div data-pages="card" class="card card-default card-collapsed" id="card-basic">
	<div class="card-header  ">
		<div class="card-title">
			<h6>พิมพ์เอกสาร</h6>
		</div>
		<div class="card-controls">
			<ul>
				<li>
					<a data-toggle="collapse" class="card-collapse" href="#">
						<i class="pg-arrow_minimize"></i>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="card-body" style="display: none;">
		<a href="<?php echo e(url('export/employee_longevity')); ?>" target="blank" class="btn btn-print btn-default">ข้อมูลอายุงาน</a>
		<a href="<?php echo e(url('export/employee_age')); ?>" target="blank" class="btn btn-print btn-default">อายุพนักงาน</a>

		<a href="<?php echo e(url('export/employee_resign')); ?>" target="blank" class="btn btn-print btn-default">ข้อมูลรายชื่อพนักงานการลาออก</a>
		<a href="<?php echo e(url('export/employee_tain')); ?>" target="blank" class="btn btn-print btn-default">ข้อมูลรายชื่อพนักงานทดลองงาน</a>

		<a href="<?php echo e(url('export/employee_current')); ?>" target="blank" class="btn btn-print btn-default">ข้อมูลพนักงานปัจจุบัน</a>
		<a href="<?php echo e(url('export/employee_hospital')); ?>" target="blank" class="btn btn-print btn-default">ข้อมูลสิทธิประกันสังคม</a>

		<a href="<?php echo e(url('export/employee_contact')); ?>" target="blank" class="btn btn-print btn-default">ข้อมูลติดต่อ E-mail เบอร์โทร </a>
	</div>
</div>

<div class="card">
	<div class="card-body">
		<table id="employee" class="table table-responsive table-bordered table-striped" width="100%">
			<thead>
				<tr>
					
					<th>#</th>
					<th>ภาพโปรไฟล์</th>
					<th>รหัสพนักงาน</th>
					<th>บัตรประชาชน</th>
					<th>คำนำ</th>
					<th>ชื่อ</th>
					<th>นามสกุล</th>
					<th>prename</th>
					<th>firstname</th>
					<th>surname</th>
					

					
					<th>ชื่อเล่น</th>
					<th>มือถือ</th>
					<th>ตำแหน่ง</th>
					<th>แผนก</th>
					<th>ฝ่าย</th>
					<th>สาขา</th>
					<th>ระดับ</th>
					<th>เพศ</th>
					<th>วันเกิด</th>
					<th>อายุ</th>
					

					<th>เริ่มงาน</th>
					<th>อายุงาน</th>
					<th>วันที่ครบทดลองงาน</th>
					<th>อีเมล</th>
					<th>บริษัท</th>
					<th>บัตรรับรองสิทธิ</th>
					<th>สถานะ</th>
					<th>ผู้อนุมัติลา</th>
					<th>ที่อยู่ตามบัตร</th>
					<th>ที่อยู่ปัจจุบัน</th>
					<th>งานที่รับผิดชอบ</th>
					<th>อำนาจอนุมัติ</th>
					<th>หมายเหตุ</th>
					<th></th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smoothe_hrm\resources\views/admin/employee.blade.php ENDPATH**/ ?>