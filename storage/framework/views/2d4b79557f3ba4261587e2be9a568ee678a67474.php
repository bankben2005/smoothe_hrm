<?php $__env->startSection('style'); ?>
<link rel="stylesheet" href="<?php echo e(asset('assets/plugins/select2/css/select2.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('template/condensed/assets/plugins/jquery-nestable/jquery.nestable.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script src="<?php echo e(asset('assets/plugins/select2/js/select2.min.js')); ?>"></script>
<script src="<?php echo e(asset('template/condensed/assets/plugins/jquery-nestable/jquery.nestable.js')); ?>"></script>
<script>
    $(document).ready(function () {

        var updateOutput = function (e) {
            var list = e.length ? e : $(e.target),
                output = list.data('output');
            if (window.JSON) {
                output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };

        var buildItem = function (item) {
            var html = "";
            html += "<li class='dd-item dd3-item' data-employee_id='" + item.employee_id +
                "' data-primary='" + item
                .primary + "'>";
            html += "<div class='dd-handle dd3-handle'>drag</div>";
            html += `<div class="dd3-content">
                        <label> main <input type="checkbox" class="check-primary" ` + (item.primary == 'T' ?
                "checked" : "") + `> </label>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        ` + item.employee_name + ` (` + item.job_name + `)
                        <a class="pull-right remove-list">X</a>
                    </div>`;
            if (item.children) {
                html += "<ol class='dd-list'>";
                $.each(item.children, function (index, sub) {
                    html += buildItem(sub);
                });
                html += "</ol>";
            }
            html += "</li>";
            return html;
        }

        var getStructure = function () {
            $.ajax({
                type: "get",
                url: rurl + "admin/organizationalstructure/json",
                dataType: "json",
                success: function (response) {
                    if(response.length==0){
                        $('.drag_handler_example').nestable({
                            maxDepth:25
                        });
                    }else{
                        $('#nestable-result').find('div.dd-empty').remove();
                        $('#nestable-result').html('');
                        $.each(response, function (index, item) {
                        $('#nestable-result').append('<ol class="dd-list">' + buildItem(
                            item) + '</ol>');
                        });
                        $('.drag_handler_example').nestable({
                            group: 1,
                            maxDepth:25
                        });
                    }
                    updateOutput($('#nestable-result').data('output', $(
                        'textarea#nestable-output')));
                }
            });
        }

        getStructure();

        $('.drag_handler_example').nestable({
            group: 1,
            maxDepth: 25
        });

        $('.js-example-basic-multiple').select2();

        $('.btn-get-employee').click(function (e) {
            e.preventDefault();
            if ($('.search .dd-empty').length != 0) {
                $(".search .dd-empty").remove();
                if($('.prepare').length==0){
                    $('.search').append(`<ol class="dd-list prepare"></ol>`);
                }
                
            }
            
            var employee_id = $("[name='employee_id[]']").val();
            $.ajax({
                type: "post",
                url: rurl + "admin/organizationalstructure/getlistemployee",
                data: {
                    'employee_id': employee_id
                },
                dataType: "json",
                success: function (response) {
                    $.each(response, function (indexInArray, valueOfElement) {
                        var list_item = 
                            `
                            <li class="dd-item dd3-item" data-employee_id="` + valueOfElement.employee_id + `" data-primary="T">
                                <div class="dd-handle dd3-handle">
                                    Drag
                                </div>
                                <div class="dd3-content">
                                    <label> main
                                        <input type="checkbox" class="check-primary" checked="">
                                    </label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    ` + valueOfElement.name + `
                                    (` + valueOfElement.job_name + `)
                                    <a class="pull-right remove-list">X</a>
                                </div>
                            </li>
                            `;
                        if($(".prepare").length!=0){
                            $(".prepare").append(list_item);
                        }else{
                            $(".search .dd-list").append(list_item);
                        }

                    });

                    $('#nestable-result').nestable({
                        maxDepth: 25
                    });
                    updateOutput($('#nestable-result').data('output', $(
                        'textarea#nestable-output')));
                }
            });
        });

        $(document).on('click', 'a.remove-list', function (e) {
            e.preventDefault();
            // var clist = $(this).closest('.drag_handler_example').find('.dd-item');
            var clist = $(this).closest('div.drag_handler_example');
            $(this).closest("li.dd-item").remove();
            // console.log(clist.find('li.dd-item').length);
            if (clist.find('li.dd-item').length == 0) {
                clist.find('ol').first().remove();
                clist.find('div.dd-empty').remove();
                console.log('find dd-empty');
                clist.append(`<div class="dd-empty"></div>`);
            }
            updateOutput($('#nestable-result').data('output', $('textarea#nestable-output')));
        });

        $(document).on('change', 'input.check-primary', function (e) {
            e.preventDefault();
            if ($(this).is(":checked")) {
                $(this).closest('li').data('primary', "T");
            } else {
                $(this).closest('li').data('primary', "F");
            }
            updateOutput($('#nestable-result').data('output', $('textarea#nestable-output')));
        });

        $('#nestable-result').on('change', updateOutput);

        updateOutput($('#nestable-result').data('output', $('textarea#nestable-output')));

        $('#post_structure').submit(function (e) {
            updateOutput($('#nestable-result').data('output', $('textarea#nestable-output')));
            e.preventDefault();
            $.ajax({
                type: "post",
                url: rurl + "admin/organizationalstructure",
                data: $(this).serialize(),
                // dataType: "json",
                success: function (response) {
                    console.log(response);
                    getStructure();
                    swal('ยินดีด้วย!', "บันทึกโครงสร้างองค์กรสำเร็จ!", "success");
                }
            });
        });
    });
</script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="card">
    <div class="card-header">
        <h5 class="pull-left"><?php echo e(!empty($menu)?$menu:''); ?></h5>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-5">
                <div class="card card-transparent">
                    <div class="row">
                        <div class="col-lg-12">
                            <div data-pages="card" class="card card-default" id="card-basic">
                                <div class="card-header">
                                    <div class="card-title">
                                        <p>บุคลากร</p>
                                    </div>
                                    <div class="card-controls">
                                        <ul>
                                            <li>
                                                <a data-toggle="collapse" class="card-collapse" href="#"><i
                                                        class="card-icon card-icon-collapse"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <select class="js-example-basic-multiple form-control" name="employee_id[]"
                                        multiple="multiple">
                                        <?php $__currentLoopData = $employee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->employee_id); ?>">
                                            <?php echo e($item->name); ?>

                                            <?php echo e(empty($item->group_name)?'':"($item->group_name)"); ?>

                                        </option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <a class="btn block btn-get-employee">สร้างรายการ</a>
                                    <hr>
                                    <div class="dd drag_handler_example search">
                                        <p>รายชื่อบุคลากรที่ค้นหา</p>
                                        <ol class="dd-list prepare" id="list_employee"></ol>
                                    </div>
                                    <hr>
                                    <div class="duplicate"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="card card-transparent">
                    <div class="row">
                        <div class="col-12">
                            <div data-pages="card" class="card card-default" id="card-basic">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h6><?php echo e(!empty($menu)?$menu:''); ?></h6>
                                    </div>
                                    <div class="card-controls">
                                        <ul>
                                            <li>
                                                <a data-toggle="collapse" class="card-collapse" href="#">
                                                    <i class="card-icon card-icon-collapse"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="dd drag_handler_example mb-2" id="nestable-result">
                                        <div class="dd-empty" style="min-height:30px"></div>
                                    </div>
                                    <form id="post_structure">
                                        <textarea id="nestable-output" name="structure" class="hidden"></textarea>
                                        <a class="btn btn-info" href="<?php echo e(url('admin/organizationalstructure/chart')); ?>"
                                            target="_blank">ดูโครงสร้างองค์กร</a>
                                        <button class="btn btn-primary">บันทึก</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smoothe_hrm\resources\views/admin/orgranizational_structure.blade.php ENDPATH**/ ?>